;----------------------------------------------
; Read (I2Count1 - 1) Bytes
;----------------------------------------------
I2cRead
	movwf	I2IntAddr
	movlw	I2IntAddr	; Setup Indirect Addressing
	movwf	FSR
;	clrf	err
	movlw	7			; Setup Bit Counter
	movwf	Count

;-------------------------------
	I2C_Start
;------------------------------- Send Slave Addr ------------
I2CLp
	tHDDAT
	btfsc	INDF,7		; Copy Bit, Send MSB first
	bsf		I2c_PORT,SDA
	btfss	INDF,7
	 bcf	I2c_PORT,SDA
	I2C_CLK
	rlf		INDF,F		; Get the next bit
	decfsz	Count,F
	 goto	I2CLp
	tHDDAT		   	; RW Bit = 1 = READ	; Clock RW Bit = 1 
	SDA_IN
	tR
	I2C_CLK

;Ack bit -----------------------
;Wack
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	btfsc	I2c_PORT,SDA
	 goto	I2w_AfailR
	bcf		I2c_PORT,SCL
	tLOW
	
;---------------------------- Read Data-------------------
	movf	I2Buff0,W	; make FSR Valid
	movwf	FSR
I2C_R0
	movlw	8		; Setup Bit Counter
	movwf	Count

I2C_Rd
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	bcf		INDF,0
    btfsc 	I2c_PORT,SDA	; Copy Bit, Read MSB first
	 bsf	INDF,0
	bcf		I2c_PORT,SCL
	tLOW
	tHDDAT
	rlf		INDF,F		; Get the next bit
	decfsz	Count,F
	 goto	I2C_Rd
	rrf		INDF,F		; Fix byte, not rotate bit0
	decfsz	BuffCount,W	; if I2Count1 > 1
	 goto	Rack			; then Ack

I2c_NotAck
; Leave SDA=input=High and clock
	tR
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	bcf		I2c_PORT,SCL
	tLOW	 
;	movf	TMR0,W    ; wait for slave to release SDA
;	movwf	Temp
;	btfsc	I2c_PORT,SDA
;	 goto	$+6
;	movf    TMR0,W
;	subwf   Temp,W
;;	btfss   STATUS,C	; if TMR0 > Temp then return
;	 goto   I2C_RdDone
;	goto	$-6
	tLOW
	tHDDAT				; wait for Data Hold Time
   	SDA_OUT					; Make Port Output = 0
	bcf		I2c_PORT,SDA
		 
I2C_RdDone
	tSUDAT	 
	SDA_OUT
	tHDDAT	
	I2C_Stop	;Stop Condition
	retlw	0

;-----------------------------------
Rack					;Ack bit
;should test the line for slave release
;with loop and timout = 32us
;	movf	TMR0,W    ; TMR0 used for timout
;	movwf	Temp
;	btfsc	I2c_PORT,SDA
;	 goto	$+6
;	movf    TMR0,W
;	subwf   Temp,W
;	btfss   STATUS,C	; if TMR0 > Temp then return
;	 goto   I2C_RdDone
;	goto	$-6
	tLOW
	tHDDAT	
	SDA_OUT
	bcf		I2c_PORT,SDA	; 0 = ACK
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	bcf		I2c_PORT,SCL
	tLOW
	tHDDAT
	SDA_IN
	movf	FSR,W		; if Low nybble != 15 then inc
	andlw	15
	xorlw	15
	btfss	STATUS,Z
	 goto	RInD
	btfsc	FSR,4
	 goto	Ra16
RInD
	incf	FSR,F
	goto	$+3
Ra16
	movlw	17
	addwf	FSR,F
	decf	BuffCount,F
	goto	I2C_R0

;-----------------------------------
;
; If fail resend data upto n times
;
;-----------------------------------
I2w_AfailR
;	incf	err,F		;increment Err.counter
;	movlw	4			; retry n-1 times
;	subwf	err,W
;	btfsc	STATUS,Z
;	 goto	I2w_Return

;	SDA_OUT
;	movlw	RTC_Addr	; get slave address
;	movwf	I2IntAddr
;	movlw	8			; Setup Bit Counter
;	movwf	Count
;	goto	I2CLp

I2C_FailR
;	movf	FSR,W		; save FSR
;	movwf	Temp
;	movlw	err
;	movwf	FSR
;	incf	err,F		;increment Err.counter
;Send Slave Arr Fail

;	movlw	4			; retry n-1 times
;	subwf	err,W
;	btfsc	STATUS,Z
;	 goto	I2r_Return

;	SDA_OUT
;	movlw	RTC_Addr	; get slave address
;	movwf	I2IntAddr
;	movlw	8			; Setup Bit Counter
;	movwf	Count
;	movf	Temp,W		;restore FSR
;	movwf	FSR
;	goto	I2CLp1		; Retry send Address

I2r_Return
	btfsc	I2c_PORT,SCL
	 bcf	I2c_PORT,SCL
	tLOW
	I2C_Stop	;Stop Condition
	retlw	1	
