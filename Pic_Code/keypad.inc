;-- Switch -----------
; All Input at Default
SWPORT equ PORTC
SW_Test		equ	0
SW_Snooze	equ	1
SW_Sleep	equ	2
SW_Hour		equ	3
SW_Alarm	equ 5
SW_Min		equ	4
SW_Time		equ	6

 CBLOCK 52
PSwitches	; Holds Pressed and Debounced Switches
_PSwitches
RSwitches	; Hold Released and Debounced Switches
_RSwitches
SW_TRIS
SW_RepeateDly
;SW_FLAGS	; 0 = PSwitches
			; 1 = _PSwitches
			; 2 = RSwitches
			; 3 = _RSwitches
 endc

; Rotate copy Carry Example
;	btfsc	SW_FLAGS,0		; Copy carry bit
;	 bsf	STATUS,C
;	rlf		PSwitches,F
;	btfsc	STATUS,C
;	 bsf	SW_FLAGS,0
;
;
;-------------------------------------------------------------
;
; Input Low voltage Min=VSS Max=.8v
; Input High Voltage Min=2  Max=VDD
;
;
; 1)-------+-------------+-------+------+-----+-----+
;          |_            |_      |_     |_    |_    |_
;          3             2       6      7     4     5
; 	 	  Time         Alarm  sleep   snooze  mn    hr
;    
; |_ = switch
;
;
;  Pin1
;    |
;	 +--------+--->RC0
;	 | 103	  |
;  __+__	__+__
;  _____	 /_\	1N4148
;    |		  |
;    |        |
;    |--------+
;  -----
;   ---
;    -
;
;
; Test Pins one at a time for press, IO table
;
;	RC=	  0	  1   2	  3	  4	  5	   6   Count = @tRise
;		++++++++++++++++++++++++++++++
;Time	|	|	|	|	|	|	| O	 |  6
;		++++++++++++++++++++++++++++++
;Alarm	|	|	|	|	|	| O	|	 |  5
;		++++++++++++++++++++++++++++++
;Min	|	|	|	|	| O	|	|	 |  4
;		++++++++++++++++++++++++++++++
;Hour	|	|	|	| O	|	|	|	 |  3
;		++++++++++++++++++++++++++++++
;Sleep	|	|	| O	|	|	|	|	 |  2
;		++++++++++++++++++++++++++++++
;Snooze |	| O	|	|	|	|	|	 |  1
;		++++++++++++++++++++++++++++++
;
; Here is the function flow for testing the switches
;	Set Tris Reg -> wait for cap to charge -> Test Cap voltage
; Cap Voltage = 1 -> Switch is pressed
; Cap Voltage = 0 -> Switch isn't pressed
;
; Set Tris all input -> test Cap
;
; Cap Voltage = 1 -> Cap Not discharged
; Cap Voltage = 0 -> Cap discharged -> Test next Switch
;
; PORTC bit 7 is don't care, because the pin is broken on my pic:<

tC_Rise	equ	16	; RC Rise Time
tC_Fall	equ	16	; RC FAll Time


keypad
	movlw	PSwitches	; Setup Memory
	movwf	FSR
	movf	PSwitches,W	; Backup Last sample
	movwf	_PSwitches
	movf	RSwitches,W
	movwf	_RSwitches
	clrf	PSwitches
	clrf	RSwitches
;	clrf	SW_FLAGS
	movlw	6
	movwf	Count
	movlw	b'01111111'	; Make port all input
	movwf	SW_TRIS

tFall
	movlw	b'01111111'
;	movwf	SWPORT
	TRIS	SWPORT
	movlw	255
	movwf	SWPORT
	movlw	tC_Fall				; wait for cap to discharge
	addwf	TMR0,W
	movwf	Temp
	movf	Temp,W
	xorwf	TMR0,W
	btfss	STATUS,Z
	 goto	$-3
	btfsc	SWPORT,SW_Test	; is it discharged?
	 goto	tFall+4

	bsf		STATUS,C		; Make sure carry doesn't shift in a 0
	rrf		SW_TRIS,F
	movf	SW_TRIS,W
	TRIS	SWPORT
	movlw	255
	movwf	SWPORT
	movlw	tC_Rise			; wait for voltage to rise
	addwf	TMR0,W
	movwf	Temp
	movf	Temp,W
	xorwf	TMR0,W
	btfss	STATUS,Z
	 goto	$-3

	bcf		STATUS,C		; make sure 1 doesn't accadentially get
	rlf		PSwitches,F		;  set to the switches
	bcf		STATUS,C		; make sure 1 doesn't accadentially get
	rlf		_PSwitches,F
	bcf		STATUS,C		; make sure 1 doesn't accadentially get
	rlf		RSwitches,F		; rotate Released switch and copy Carry bit

tRise
	nop
	btfsc	SWPORT,SW_Test
	 bsf	PSwitches,1

;------- Process Released Switches ---------------------------------------
	btfss	_PSwitches,7	; if switch was pressed last time then
	 goto	tRDone
	btfss	SWPORT,SW_Test	; if Test pin is clear then set released switch
	 bsf	RSwitches,1

tRDone
	decfsz	Count,F
	 goto	tFall

	movf	SW_RepeateDly,W	; Switch Repeat Delay
	xorlw	0
	btfsc	STATUS,Z
	 retlw	0			; if = 0 then return
	movf	SW_RepeateDly,W	; Switch Repeat Delay
	addwf	TMR0,W
	movwf	Temp
	movf	Temp,W
	xorwf	TMR0,W
	btfss	STATUS,Z
	 goto	$-3
	retlw	0

