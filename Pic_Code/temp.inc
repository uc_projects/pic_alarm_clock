;
; Display �C
;
; Stored Temperature * .25�C resolution = Temperature�C
;
;
; The Multiplication could be done by a loop but it'd take
; a couple of millisec's to finish the calculation
;
; 2^10 * cycle Time of .2us = 2.048e-4 * 8 cycles = 1.6384ms
;
;
;
; (Temperature=-Temp - 4) -> Temp=0? ->y Done
;			     	     ->N incf Temp�C
;
	movlw	MSB_Temp
	movwf	FSR
	movlw	LSB_Temp
	movwf	FSR

