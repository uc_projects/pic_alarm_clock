;_Status is used as a pointer to data
;
;	movf	_Status,W	; this reg contains address of data
;	movwf	FSR
;

KP_Alarm0
	movlw	A0DOW
	goto	$+2
KP_Alarm1
	movlw	A1DOW
	movwf	_Status
	movwf	FSR
	bsf		INDF,0		; Default is alarm on day
	movlw	PSwitches	; Don't skip
	movwf	FSR
	clrf	PSwitches
	movlw	Sec			; start by asking to alarm on Sun?
	movwf	FSR
	movlw	1
	movwf	Day
	clrf	Sec			; Display Seconds as 00
	Page0				; Beep
	call	BEEP_625
	call	BEEP_675
	Page1
	call	ClrDisp
KP_SetDayLP
	bsf		STATUS,PA0	; page 1
	bcf		STATUS,PA1
	movlw	0
	call	LcdAd
	call	DispDay	;this function sets FSR to Day
	movlw	'?'
	call	LCDChar
	movf	_Status,W
	movwf	FSR
	movlw	'Y'
	btfss	INDF,0
	 movlw	'N'
	call	LCDChar
	movlw	4
	call	LcdAd

SetAlarm_DayLp
	Page0
	call	keypad
	Page3
	btfsc	RSwitches,SW_Min
	 goto	SetAlarmDayInc
	btfsc	RSwitches,SW_Hour
	 goto	SetAlarmDayDec
	btfss	RSwitches,SW_Snooze
	 goto	SetAlarm_DayLp

SetAlarmDayDone
	Page0
	call	BEEP_675
	call	BEEP_625
	Page3
	call	IncDay
	movf	Day,W	;if = 1 then overflowed, Next
	xorlw	1
	btfsc	STATUS,Z
	 goto	SetAlarmTime
	movf	_Status,W
	movwf	FSR
	rlf		INDF,F
	bsf		INDF,0		; Default is alarm on day
	goto	KP_SetDayLP

SetAlarmDayInc
	Page0
	call	BEEP_625
	Page3
	movf	_Status,W
	movwf	FSR
	bsf		INDF,0
	goto	KP_SetDayLP

SetAlarmDayDec
	Page0
	call	BEEP_675
	Page3
	movf	_Status,W
	movwf	FSR
	bcf		INDF,0
	goto	KP_SetDayLP

SetAlarmTime
	movlw	RoutineSel	; Set Hour
	movwf	FSR
	movlw	4
	movwf	RoutineSel
	bsf		STATUS,PA0	; page 1
	bcf		STATUS,PA1
	call	ClrDisp
	call	DispHr
	call	DispMin
;	call	DispSec
	bsf		STATUS,PA0	; page 1
	bcf		STATUS,PA1
	call	LCDaddStart
	call	LcdAd
	call	rtcDisp
	call	LCDaddStart
	call	LcdAd

;--------------------------------------------------
SetAlarm_TimeLp
	Page0
	call	keypad
	Page3
	btfsc	RSwitches,SW_Min
	 goto	SetAlarm_TimeInc
	btfsc	RSwitches,SW_Hour
	 goto	SetAlarm_TimeDec
	btfss	RSwitches,SW_Snooze
	 goto	SetAlarm_TimeLp

SetAlarm_TimeDone
	Page0
	call	BEEP_675
	call	BEEP_625
	Page3
	movlw	RoutineSel
	movwf	FSR
	movlw	5
	xorwf	RoutineSel,W
	btfsc	STATUS,Z
	 goto	AlarmSave
	incf	RoutineSel,F
	goto	SetAlarm_TimeLp

SetAlarm_TimeInc
	Page0
	call	BEEP_625
	Page3
	call	Inc_Time	; this updates FSR
	goto	SetAlarm_TimeLp-7

SetAlarm_TimeDec
	Page0
	call	BEEP_675
	Page3
	call	Dec_Time	; this updates FSR
	goto	SetAlarm_TimeLp-7

AlarmSave
	movlw	Sec		; Copy Time Registers
	movwf	FSR
	movf	Hour,W
	movwf	_High
	movf	Min,W
	movwf	_Low
	incf	_Status,W	; now Copy to Alarm Registers
	movwf	FSR
	movf	_Low,W
	movwf	INDF
	incf	FSR
	movf	_High,W
	movwf	INDF

;-----------------------------------------------------------
AlarmWriteTest
;if Day of week matches then update alarm
	movlw	Day
	movwf	FSR
	movf	Day,W
	movwf	Count
	movwf	BuffCount
	movlw	A0DOW
	movwf	FSR
	movf	A0DOW,W
	movwf	_Low
	movf	A1DOW,W
	movwf	_High

AWT_Loop
	movlw	0
	movwf	Temp
	btfsc	_Low,6
	 movlw	2
	addwf	Temp,F
	btfsc	_High,6
	 movlw	1
	addwf	Temp,F
	rlf		_Low,F
	rlf		_High,F
	decfsz	Count,F
	 goto	AWT_Loop

; if Temp=0 then clear alarm registers, disable alarms
; 1 = write Alarm0, 2 = write alarm1
	Page0
	movf	Temp,W		; 4 = alarm1
	xorlw	4
	btfsc	STATUS,Z
	 goto	WriteA1
	movf	Temp,W		; 1 = alarm2
	xorlw	1
	btfsc	STATUS,Z
	 goto	WriteA2
	movf	Temp,W		; 3 = alarm1 & 2
	xorlw	3
	btfsc	STATUS,Z
	 goto	WriteA12
;	movf	Temp,W	; 0 = disable Alarms
;	xorlw	0
;	btfsc	STATUS,Z
	goto	DisableAlarms
;	goto	AlarmSound




