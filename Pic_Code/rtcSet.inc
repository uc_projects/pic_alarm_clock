;include SetDefault.inc

; These Routines increment and decrement BCD coded Registers
; some of which have propryitary bits, ie(Hour)

Inc_Time
	movlw	RoutineSel
	movwf	FSR
	movlw	7			; make sure value is within range
	subwf	RoutineSel,W
	btfss	STATUS,C
	 goto	$+3
	movlw	0
	goto	$+2
	movf	RoutineSel,W
	addwf	PCL,F
	goto	IncYear	 ;6
	goto	IncMonth ;5
	goto	IncDate	 ;4
	goto	IncDay	 ;3
	goto	IncHour  ;0
	goto	IncMin	 ;1
	goto	IncSec	 ;2

Dec_Time
	movlw	RoutineSel
	movwf	FSR
	movlw	7			; make sure value is within range
	subwf	RoutineSel,W
	btfss	STATUS,C
	 goto	$+3
	movlw	0
	goto	$+2
	movf	RoutineSel,W
	addwf	PCL,F
	goto	DecYear	 ;6
	goto	DecMonth ;5
	goto	DecDate	 ;4
	goto	DecDay	 ;3
	goto	DecHour  ;0
	goto	DecMin	 ;1
	goto	DecSec	 ;2

;------------------------------------------------
DecDate
	movlw	61
	movwf	Count
	call	IncDate
	decfsz	Count
	 goto	$-2
	retlw	0
IncDate
	movlw	Date
	movwf	FSR
	movf	INDF,W
	xorlw	b'00110001' ; if=31 clear
	btfsc	STATUS,Z	; if = reset, else inc
	 goto	ClrDate
	movf	INDF,W		;inc High
	andlw	b'00001111'	;check low for=9
	xorlw	b'00001001'
	btfss	STATUS,Z
	 goto	IncDateret
	swapf	INDF,W		;Inc High
	andlw	b'00001111'
	movwf	INDF
	incf	INDF,F
	swapf	INDF,F
	retlw	0

ClrDate
	movlw	1
	movwf	INDF
	retlw	0
IncDateret
	incf	INDF,F
	retlw	0

;------------------------
DecDay
	movlw	13
	movwf	Count
	call	IncDay
	decfsz	Count
	 goto	$-2
	retlw	0
IncDay
	movlw	Day
	movwf	FSR
	incf	Day,W
	xorlw	8
	btfsc	STATUS,Z
	 clrf	Day
	movlw	1
	addwf	Day,F
	retlw	0
	
;-------------------------------------------------------------------------------
DecHour
	movlw	23
	movwf	Count
	call	IncHour
	decfsz	Count,F
	 goto	$-2
	retlw	0

IncHour
	movlw	Hour
	movwf	FSR
	bsf		Hour,6	; make always 12hour

	incf	Hour,W	; 9:00 - 10:00 --------------------------------
	andlw	b'00011111'	;mask out the Bits, AM/PM and 12/24
	xorlw	b'00001010'	; if = 9 then, next=10
	btfss	STATUS,Z
	 goto	$+6
	movf	Hour,W
	andlw	b'01110000'
	movwf	Hour
	bsf		Hour,4
	retlw	0

	incf	Hour,W	; 12:00-1:00 ----------------------------------
	andlw	b'00011111'	;mask out the Bits, AM/PM and 12/24
	xorlw	b'00010011'	; if = 12 then, next=1, and toggle am/pm
	btfss	STATUS,Z
	 goto	$+7
	comf	Hour,W		;Toggle am/pm
	andlw	b'00100000'
	movwf	Hour
	movlw	b'01000001'	; 1:00, 12hour format
	addwf	Hour,F
	retlw	0
	incf	Hour,F	;1 to 8:00
	retlw	0

;-------------------------------------------------------------------------------
DecSec
	movlw	119
	movwf	Count
	call	IncSec
	decfsz	Count,F
	 goto	$-2
	retlw	0

DecMin
	movlw	119
	movwf	Count
	call	IncMin
	decfsz	Count,F
	 goto	$-2
	retlw	0

IncSec	; Secound is same routine differant register
	movlw	Sec
	goto	$+2
IncMin
	movlw	Min
	movwf	FSR
	movf	INDF,W
	andlw	b'00001111'	;check low for=9
	xorlw	b'00001001'
	btfss	STATUS,Z
	 goto	IncMinret
	swapf	INDF,W		;inc High
	andlw	b'00001111'
	xorlw	b'00000101' ; if=5clear
	btfsc	STATUS,Z	; if = reset, else inc
	 goto	ClrMinret
	swapf	INDF,W		;Inc High
	andlw	b'00001111'
	movwf	INDF
	incf	INDF,F
	swapf	INDF,F
	retlw	0
ClrMinret
	clrf	INDF
	retlw	0
IncMinret
	incf	INDF,F
	retlw	0

;-------------------------------------------------------------------------------
DecMonth
	movlw	23
	movwf	Count
	call	IncMonth
	decfsz	Count,F
	 goto	$-2
	retlw	0

IncMonth
	movlw	Month
	movwf	FSR
	movf	INDF,W		;inc High
	andlw	b'00011111'
	xorlw	b'00010010' ; if=12clear
	btfsc	STATUS,Z	; if = reset, else inc
	 goto	ClrMonthret
	movf	INDF,W
	andlw	b'00001111'	;check low for=9
	xorlw	b'00001001'
	btfss	STATUS,Z
	 goto	IncMonthret
	swapf	INDF,W		;Inc High
	andlw	b'00001111'
	movwf	INDF
	incf	INDF,F
	swapf	INDF,F
	retlw	0
ClrMonthret
	movlw	1
	movwf	INDF
	retlw	0
IncMonthret
	incf	INDF,F
	retlw	0

;-------------------------------------------------------------------------------
DecYear
	movlw	199
	movwf	Count
	call	IncYear
	decfsz	Count,F
	 goto	$-2
	retlw	0

IncYear
	movlw	Year
	movwf	FSR
	movf	INDF,W		;inc High
	xorlw	b'10011001' ; if=99clear
	btfsc	STATUS,Z	; if = reset, else inc
	 goto	ClrYearret
	movf	INDF,W
	andlw	b'00001111'	;check low for=9
	xorlw	b'00001001'
	btfss	STATUS,Z
	 goto	IncYearret
	swapf	INDF,W		;Inc High
	andlw	b'00001111'
	movwf	INDF
	incf	INDF,F
	swapf	INDF,F
	retlw	0
ClrYearret
	clrf	INDF
	retlw	0
IncYearret
	incf	INDF,F
	retlw	0
