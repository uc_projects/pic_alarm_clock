;  TOCKI is fed with a 32khz clock from a TXCO RTC chip
; there is no prescaler so Timer0 increments every 31us
; This should be useful to make a software timer.
; By Looking at the Clock Source with my oscillascope
; I measured the high and low times to be.
;
; Thigh = 8uS
; Tlow 	= 23us
;
; Clearing the Timer inhibits it's operation for two cycles
; not including the time to write the register
;
; Since the chip hasn't a interrupt for Timer Overflow it
; Must Be done through Software Polling for continuous 
; Timer operation
;
;	movf	TMR0,W
;	xorlw	255
;	btfsc	STATUS,C
;	 clrf	TMR0

