;--------------------------------------------
; This routine should be called by LCDInit
;
;
; CRROM address = 5bits = 32locations
; there are 8bytes per char so 32/8 = 4 chars
; the address is auto incremented
;
;
; 	movlw	b'xxx00000'
; 	movlw	b'xxx00000'
; 	movlw	b'xxx00000'
; 	movlw	b'xxx00000'
; 	movlw	b'xxx00000'
; 	movlw	b'xxx00000'
; 	movlw	b'xxx00000'
;
; 	movlw	b'xxx11111'
; 	movlw	b'xxx11111'
; 	movlw	b'xxx01010'
; 	movlw	b'xxx01010'
; 	movlw	b'xxx01010'
; 	movlw	b'xxx11111'
; 	movlw	b'xxx11111'
;
;
DispCustomChar
	movlw	b'01000000'	;CGROM Address
	call	LCDIns
	movlw	b'00011111'	; each custom char
	call	LCDChar
	movlw	b'00011111' ; takes 8 bytes
	call	LCDChar
	movlw	b'00001000'
	call	LCDChar
	movlw	b'00001000'
	call	LCDChar
	movlw	b'00001000'
	call	LCDChar
	movlw	b'00011111'
	call	LCDChar
	movlw	b'00011111'
	call	LCDChar
	movlw	b'00000000'
	call	LCDChar

	movlw	b'01000000'	;CGROM Address
	call	LCDIns
	movlw	b'00011111'	; each custom char
	call	LCDChar
	movlw	b'00011111' ; takes 8 bytes
	call	LCDChar
	movlw	b'00000010'
	call	LCDChar
	movlw	b'00000010'
	call	LCDChar
	movlw	b'00000010'
	call	LCDChar
	movlw	b'00011111'
	call	LCDChar
	movlw	b'00011111'
	call	LCDChar
	movlw	b'00000000'
	call	LCDChar

	movlw	b'00011111'	; each custom char
	call	LCDChar
	movlw	b'00011111' ; takes 8 bytes
	call	LCDChar
	movlw	b'00001010'
	call	LCDChar
	movlw	b'00001010'
	call	LCDChar
	movlw	b'00001010'
	call	LCDChar
	movlw	b'00011111'
	call	LCDChar
	movlw	b'00011111'
	call	LCDChar
	movlw	b'00000000'
	call	LCDChar
	retlw	0
