;----------------------------------------------
; Char Codes
;
; Each Char Code is comprised of 5 bytes.
;
; So to Read char('A') from the table you'd
; set the w reg to the Offset of where the code
; starts, then read 5 bytes.
;
; The Table follows ASCII Char Code Order.
; So to get the offset of a given char
; you multiply the ASCII Code by 5.
;
; The table starts at char code 33 or char('!')
;Lookup offset address = (char_Code - 33)x5
;
;----------------------------------------------
; You can see the chars in the code by looking
; at the table sideways.
;----------------------------------------------

CharROM
	addwf	PCL,F
;Quotation Marks
	retlw b'00000000'	; !
	retlw b'00000000'
	retlw b'01011111'
	retlw b'00000000'
	retlw b'00000000'

	retlw b'00000011'	; "
	retlw b'00000000'
	retlw b'00000011'
	retlw b'00000000'
	retlw b'00000000'

	retlw b'00010100'	; #
	retlw b'01111111'
	retlw b'00010100'
	retlw b'01111111'
	retlw b'00010100'

	retlw b'01001100'	; $
	retlw b'01010010'
	retlw b'01011111'
	retlw b'00100010'
	retlw b'00000000'

	retlw b'00000000'	; %
	retlw b'01111110'
	retlw b'10000001'
	retlw b'01111110'
	retlw b'00000000'

	retlw b'00000000'	; &
	retlw b'01111110'
	retlw b'10000001'
	retlw b'01111110'
	retlw b'00000000'

;Numbers
	retlw b'00000000'	; 0
	retlw b'01111110'
	retlw b'10000001'
	retlw b'01111110'
	retlw b'00000000'

	retlw b'00000000'	; 1
	retlw b'10000010'
	retlw b'11111111'
	retlw b'10000000'
	retlw b'00000000'

	retlw b'00000000'	; 2
	retlw b'11000010'
	retlw b'10100001'
	retlw b'10011110'
	retlw b'00000000'

	retlw b'00000000'	; 3
	retlw b'10010001'
	retlw b'10010001'
	retlw b'01101110'
	retlw b'00000000'

	retlw b'00000000'	; 4
	retlw b'00011000'
	retlw b'00010010'
	retlw b'11111111'
	retlw b'00000000'

	retlw b'00000000'	; 5
	retlw b'10001111'
	retlw b'10001001'
	retlw b'01110001'
	retlw b'00000000'

	retlw b'00000000'	; 6
	retlw b'01111110'
	retlw b'10001001'
	retlw b'01110001'
	retlw b'00000000'

	retlw b'00000000'	; 7
	retlw b'11100001'
	retlw b'00011001'
	retlw b'00000111'
	retlw b'00000000'

	retlw b'00000000'	; 8
	retlw b'01110110'
	retlw b'10001001'
	retlw b'01110110'
	retlw b'00000000'

	retlw b'00000000'	; 9
	retlw b'10001110'
	retlw b'10010001'
	retlw b'01111110'
	retlw b'00000000'
