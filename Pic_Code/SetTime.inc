
KP_SetTime
;	movlw	PSwitches	
;	movwf	FSR
	clrf	PSwitches	; Don't skip set year
	movlw	RoutineSel
	movwf	FSR
	clrf	RoutineSel
	Page0
	call	BEEP_625
	call	BEEP_675
	bsf		STATUS,PA0	; page 1
	bcf		STATUS,PA1
	call	LCDaddStart
	call	LcdAd
	call	rtcDisp
	call	LCDaddStart
	call	LcdAd

KP_SetTimeLp
	Page0
	call	keypad
	Page3
	btfsc	RSwitches,SW_Min
	 goto	KP_SetTimeInc
	btfsc	RSwitches,SW_Hour
	 goto	KP_SetTimeDec
	btfss	RSwitches,SW_Snooze
	 goto	KP_SetTimeLp

KP_SetTimeDone
	Page0
	call	BEEP_675
	call	BEEP_625
	Page3
	movlw	RoutineSel
	movwf	FSR
	movlw	6
	xorwf	RoutineSel,W
	btfss	STATUS,Z
	 goto	KP_SetTimeDone1
	Page2
	call	WriteTime
	Page0
	goto	SetTime_Done
KP_SetTimeDone1
	incf	RoutineSel,F
	Page3
	goto	KP_SetTime+4

KP_SetTimeInc
	Page0
	call	BEEP_625
	Page3
	call	Inc_Time	; this updates FSR
	goto	KP_SetTimeLp-7

KP_SetTimeDec
	Page0
	call	BEEP_675
	Page3
	call	Dec_Time	; this updates FSR
	goto	KP_SetTimeLp-7

