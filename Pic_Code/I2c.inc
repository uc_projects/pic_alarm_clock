; Ack Polling returna 1 to indicate an error, 0 on Success
;
;------------------------------ I2C Routines --------------------

 CBLOCK 59
err
I2IntAddr	; I2C Regs.
BuffAddr
I2Buff0
 endc

;--- RTC ------------------------------------------------------
; Output
I2c_PORT equ PORTE
SCL equ 6
SDA equ 7


;---------------------------- Timing Macros --------------------
;-- time between START & STOP Conditions
; Fast = 1.3uS
; Slow = 4.7uS
I2C_tBuf macro
 goto	$+1
 goto	$+1
 goto	$+1
 nop
 endm

;-- Hold time repeated RESTART Condition
; Fast = .6uS
; Slow = 4.0uS
tHDSTA macro
 goto	$+1
 nop
 endm

;-- Start Setup time
; Fast = .6uS
; Slow = 4.7uS
tSUSTA macro
 I2C_tBuf
 endm

;-- Setup Stop time
; Fast = .6uS
; Slow = 4.7uS
tSUSTO macro
 I2C_tBuf
 endm

;Clk low time
; Fast = 1.3uS
; Slow = 4.7uS
tLOW macro
 I2C_tBuf
 endm

;Clk high time
; Fast = 1.3uS
; Slow = 4.7uS
tHIGH macro
 I2C_tBuf
 endm

;Data Setup Time
; .1
; .25
tSUDAT macro
 nop
 endm

;Data Hold Time
; .9us
; .9us
tHDDAT macro
 endm

tR macro
 nop
 endm

;--------------------------- I/O Macros -------------------------
I2c_SDA_In	equ b'10000000'	; Default State, with SDA=input
I2c_SDA_Out	equ b'00000000'

SDA_OUT macro
	movlw	I2c_SDA_Out	; SDA = output
	TRIS	I2c_PORT
 endm	


SDA_IN macro
	movlw	I2c_SDA_In	; SDA=input, RW = 1 because of Pullup
	TRIS	I2c_PORT
 endm

I2C_CLK macro
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	bcf		I2c_PORT,SCL
	tLOW
 endm

;I2c Start Condition, assumes that SDA and SCL are output
I2C_Start macro
	I2C_tBuf
	bcf		I2c_PORT,SDA	; Start Condition
	tHDSTA
	bcf		I2c_PORT,SCL
	tLOW
 endm

;I2c Stop Condition, assumes that SDA and SCL are output
I2C_Stop macro
	SDA_OUT
	nop
	bcf		I2c_PORT,SDA
	tSUDAT
	tLOW
	bsf		I2c_PORT,SCL
	tSUSTO
	bsf		I2c_PORT,SDA	; Start Condition
 endm

 include	I2CWrite.inc	 ; RTC DS3231
 include	I2CRead.inc		 ; RTC DS3231

I2c_AckErr
;	movlw	err
;	movwf	FSR
;	movf	err,W
;	xorlw	20
;	btfss	STATUS,Z
;	 goto	$+3
;	movf	_High,W
;	movwf	PCL
;	incf	err,F
;	movf	_Low,W	; PC address + 1
;	movwf	PCL

