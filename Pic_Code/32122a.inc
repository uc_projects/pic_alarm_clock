;----------------------------------------------
;
; This file is for the 122x32 graphic Display
;the code is for pic16F59.
;
;----------------------------------------------
;
;  Fonts are loaded from EEPROM
; I used 93LC66B
;
;
;

;-- LCD Literals --------------
LCG_Ctrl equ PORTB
LCG_E1	equ 2
LCG_E2	equ 3
LCG_RW	equ 4
LCG_A0	equ 5
LCG_Data equ PORTC
LCG_RST equ 7		; this is for graphic LCD

;LCD Registers
; SLCD_Address	; shadow Register for LCD Address
; SLCD_Flags	; Keeps track of Page
;LCDTemp
;LCDCount
;LCDCountHi
;----------------------------------------
LCD_Clock macro
	call	Dlay2
	bsf		LCG_Ctrl,LCG_E1	 ; Clock The Data
	bsf		LCG_Ctrl,LCG_E2
	call	Dlay20
	bcf		LCG_Ctrl,LCG_E1
	bcf		LCG_Ctrl,LCG_E2
	call	Dlay20
 endm

LCD_Clock1  macro
	call	Dlay2
	bsf		LCG_Ctrl,LCG_E1	 ; Clock The Data
	call	Dlay20
	bcf		LCG_Ctrl,LCG_E1
	call	Dlay20
 endm

LCD_Clock2  macro
	call	Dlay2
	bsf		LCG_Ctrl,LCG_E2	 ; Clock The Data
	call	Dlay20
	bcf		LCG_Ctrl,LCG_E2
	call	Dlay20
 endm

;----------------------------------------------------------------
; Clear Display Data Ram
;----------------------------------------------------------------
ClrGisp
	bcf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bcf		LCG_Ctrl,LCG_A0
	movlw	b'10111000'		 ; Page Addr Set : 0
	movwf	LCD_Data
	movwf	LCDTemp
	LCD_Clock

	movlw	4			; 4 pages of Ram
	movwf	LCDCountHi

ClrDispLp
	movlw	61			; Double Clock 61 bytes
	movwf	LCDCount

	bcf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bsf		LCG_Ctrl,LCG_A0
	clrf	LCD_Data		 ; clear Data Port
	LCD_Clock

	decfsz	LCDCountHi,F		 ; Clock 4 Pages
	 retlw	0
	bcf		LCG_Ctrl,LCG_A0
	incf	LCDTemp,F			 ; Inc. Page
	movf	LCDTemp,W
	movwf	LCG_Data
	LCD_Clock
	goto	ClrDispLp

;-- Inialize the Display ----------------------------------------
; Upon Powerup the display shows random pixels enabled.
;----------------------------------------------------------------
LCGInit	; Reset Display via hardware Pin???
	bcf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bcf		LCG_Ctrl,LCG_A0

	movlw	b'10101110'	; Disp On/Off : OFF
	movwf	LCG_Data
	LCD_Clock
	movlw	b'11000000'	; Display Start Line: 1
	movwf	LCG_Data
	LCD_Clock
	movlw	b'10100100'	; STATIC Drive On/Off : Off
	movwf	LCG_Data
	LCD_Clock
	movlw	b'00000000'	; Column addr. Set : 0
	movwf	LCG_Data
	LCD_Clock
	movlw	b'10111011'	; Page Addr Set : 1
	movwf	LCG_Data
	LCD_Clock
	movlw	b'10101001'	; Duty Selest : 1/32
	movwf	LCG_Data
	LCD_Clock
	movlw	b'10100000'	; ADC SElect : Rightward output
	movwf	LCG_Data
	LCD_Clock
	movlw	b'11101110'	; Read Modify Write : Off (END)
	movwf	LCG_Data
	LCD_Clock
;	goto	ClrDisp
	retlw	0

;-----------------------------------------------------------------
; Display Routines
;-----------------------------------------------------------------
LCGChar
;Get Table Offset
;W = (W - 33)x5: sub 33 and add W to itself 5 times
	movwf	LCDTemp	;Temp = W - 33
	movlw	33
	subwf	LCDTemp,F
	movf	LCDTemp,W	;Temp = Temp x 5
	addwf	LCDTemp,F
	addwf	LCDTemp,F
	addwf	LCDTemp,F
	addwf	LCDTemp,F	;Temp = char Offset

	movlw	5		; 1 char = 5 bytes
	movwf	LCDCount
LCDCharLp
	movf	LCDTemp,W
	bsf		STATUS,PA0	; Char Table is on Page 3
	bsf		STATUS,PA1	;
	call	CharROM
	bcf		STATUS,PA0
	bcf		STATUS,PA1

	movwf	LCG_Data		 ; Write the Data Port
	bcf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bsf		LCG_Ctrl,LCG_A0
	LCD_Clock1

	incf	LCDTemp,F			; Get next byte from table
	decfsz	LCDCount,F
	 goto	LCDCharLp
	retlw	0

;-----------------------------------------------------------------
; Get Decimal Value and return W with ASCII Char
; displays BCD number ie(0-9)
;-----------------------------------------------------------------
GispNum
; 	addlw	48
	movwf	LCDTemp
;	sublw	58
	btfsc	STATUS,Z
	 bcf	STATUS,C
	btfsc	STATUS,C
	 goto	LCDChar + 1
	movlw	7
	addwf	LCDTemp,F
	goto	LCDChar + 1

LCGspac	movlw	' '
	goto	LCDChar

LcgAd	; Column Address Set = 0, address = 0-79
LCGIns	movwf	LCG_Data
	bcf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bcf		LCG_Ctrl,LCG_A0	
	LCD_Clock1
	retlw	0



;LCDTemp = Data read from LCD
LCGRdStatus
	bsf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bcf		LCG_Ctrl,LCG_A0
	clrf	LCG_Data		; clear the latches
	movlw	255				; make data input
	TRIS	LCG_Data
	call	Dlay2
	bsf		LCG_Ctrl,LCG_E1	 ; Clock The Data
	call	Dlay20
	movf	LCD_Data,W
	movwf	LCDTemp
	bcf		LCG_Ctrl,LCG_E1
	call	Dlay20
	movlw	0				; make data output
	TRIS	LCG_Data
	retlw	0

;LCDTemp = Data read from LCD
LCDRdData
	bsf		LCG_Ctrl,LCG_RW ; Set Write Display Data
	bsf		LCG_Ctrl,LCG_A0
	clrf	LCG_Data		; clear the latches
	movlw	255				; make data input
	TRIS	LCD_Data
	call	Dlay2
	bsf		LCG_Ctrl,LCG_E1	 ; Clock The Data
	call	Dlay20
	movf	LCD_Data,W
	movwf	LCDTemp
	bcf		LCG_Ctrl,LCG_E1
	call	Dlay20
	movlw	0				; make data output
	TRIS	LCG_Data
	retlw	0
