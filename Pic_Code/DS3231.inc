;-----------------------------------------------------
;I2C Routines and Macro's
;-----------------------------------------
; I2C Pic16F59
; DS3231
;
; This Routine uses a small buffer to
; read and write data to a Real Time Clock
;
;-----------------------------------------
; I2C Pic16F59
; DS3231
;
; This Routine uses a small buffer to
; read and write data to a Real Time Clock
;
; Default Time -------------------------------------------
;
; On the first application of power (VCC) or when a valid
;I2C address is written to the part (VBAT), the time and
;date registers are reset to 01/01/00 01 00:00:00
;(MM/DD/YY DOW HH:MM:SS
;
;
; Typical oscillator startup time is less than one
;second. Approximately 2 seconds after VCC is applied,
;or a valid I2C address is written
;
; Temperature is 10 bit two�s complement format
;Upon power reset, the registers are set to a default
;temperature of 0�C
;
;
; 0�C = 0000h
; to convert to Ferinheight
; �C = Digital value *.25�C
; (�C * 1.8)+32
;
;----------------------------------------------
;RTC Address Map Literals
;----------------------------------------------
 CBLOCK 16
 ; Time Literals for RTC
Sec		;equ H'00'		; 00-59
Min		;equ H'01'		; 00-59
Hour	;equ H'02'		; 1-12 + am/pm ; 00-23 ; 12/24 bit=6

 ; Date Literals for RTC
Day		;equ H'03'		; 1-7
Date	;equ H'04'		; 01-31
Month	;equ H'05' 		; 01-12 ; century bit=7
Year	;equ H'06'		; 00-99

; Alarm 1
A1Sec	;equ H'07' 		; 00-59 ; alarm bit=7
A1Min	;equ H'08'		; 00-59 ; alarm bit=7
A1Hour	;equ H'09'		; 1-12 am/pm bit=5 ; 00-23 ; 12/24 bit=6 ; alarm bit=7
A1Date	;equ H'0A'		; 1-31/1-7 ; day/date bit=6 ; alarm bit=7

; Alarm 2
A2Min	;equ H'0B' 		; 00-59 ; alarm bit=7
A2Hour	;equ H'0C'		; 00-59 ; alarm bit=7
A2Date	;equ H'0D'		; 1-31/1-7 ; day/date bit=6 ; alarm bit=7

; Control Reg
; Status Reg
RTC_Ctl		;equ H'0E' ; b'00x11100' = PowerUp?
RTC_Stat	;equ H'0F' ; b'10001xxx' = PowerUp?
 endc

 CBLOCK 48
RTC_Offset	;equ H'10' ; Aging Offset
MSB_Temp	;equ H'11'
LSB_Temp	;equ H'12'
_Sec
 endc

RTC_Addr equ b'11010000' ; slave address of device
RTC_PORT equ PORTE
RTC_ALARM equ 4

;-- Status Register ----------------------------
OSF		equ	7	; Osscillator Stop Flag
EN32kHz	equ	3
BSY		equ	2
A2F		equ	1
A1F		equ	0

;-- Control Register ---------------------------
EOSC	equ	7
BBSQW	equ	6
CONV	equ	5
RS2		equ	4
RS1		equ	3
INTCN	equ	2
A2IE	equ	1
A1IE	equ 0
 
;---------------------------------------------------------------
ReadDS3231
	movlw	BuffAddr	; set FSR = to MEM Page
	movwf	FSR
	movlw	H'00'		; 1st RTC REgister
	movwf	BuffAddr
	clrf	BuffCount	; Just Send Address, for Read
	movlw	RTC_Addr
	call	I2cWrite

	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	Sec			; Set Save Location
	movwf	I2Buff0
	movlw	19			; Read Everything
	movwf	BuffCount
	movlw	RTC_Addr+1	; 1 = READ, 0 = write
	call	I2cRead
	retlw	0

WriteTime
;Write Clock Registers to RTC
;Sec, Min, Hour, Day, Date, Year
;
	movlw	7			; write 8 bytes
	movwf	BuffCount	; Just Send Address, for Read
	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	Sec			; Set Save Location
	movwf	I2Buff0
	movlw	H'00'		; write Sec Address
	movwf	BuffAddr
	movlw	RTC_Addr
	call	I2cWrite
	retlw	0

WriteStatCtrl
; update Status and Control
	movlw	2			; write 8 bytes
	movwf	BuffCount	; Just Send Address, for Read
	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	RTC_Ctl		; Set Save Location
	movwf	I2Buff0
	movlw	H'0E'		; write Sec Address
	movwf	BuffAddr
	movlw	RTC_Addr
	call	I2cWrite
	retlw	0


;------------ Chuck McManis -------------------------------------------------
;
;http://www.mcmanis.com/chuck/robotics/projects/pic-16bit.htm
;
; 16 bit unsigned subtraction with carry out.
; Word format is little endian (LSB at lower address)
; Operation is DST = DST - SRC
;
; (This from the "tips and tricks" seminar handout)
;
; DST is replaced, SRC is preserved, Carry is set correctly
;
;
SUB16   MACRO   DST, SRC
        MOVF    (SRC),W         ; Get low byte of subtrahend
        SUBWF   (DST),F         ; Subtract DST(low) - SRC(low)
        MOVF    (SRC)+1,W       ; Now get high byte of subtrahend
        BTFSS   STATUS,C        ; If there was a borrow, rather than
        INCF    (SRC)+1,W       ; decrement high byte of dst we inc src
        SUBWF   (DST)+1,F       ; Subtract the high byte and we're done
        ENDM
        
SUBI16  MACRO   DST, SB
        MOVLW   LOW (SB)
        SUBWF   (DST), F
        MOVLW   HIGH (SB)
        BTFSS   STATUS, C
        MOVLW   (HIGH (SB))+1
        SUBWF   (DST)+1,F
        ENDM


;--------------------------------------------------------
; Convert The 10 bit Temperature to Celcius Temperature
;
; TempStored = b'S111-1111-11xx-xxxx
; 
; TempStored - 63 = b'xxxx-xxS1-1111-1111
;
;
;  StoredTemp *.25 = Degrees C
;
Convert_TempC
;
; Subtract four from the 16 bit value
; and add 1 to Accumulator every time result is > 1
;
; _Low, _High  = 16 bit start, = accumulator
;
; Count,Temp = 16 bit subtract value
; BuffCount = result

	movlw	MSB_Temp		; setup 10 bit register
	movwf	FSR
	movf	MSB_Temp,W
	andlw	b'00111111'
	movwf	_Low
	rlf		_Low,F
	rlf		_Low,F
	btfsc	LSB_Temp,7
	 bsf	_Low,1
	btfsc	LSB_Temp,6
	 bsf	_Low,0
	clrf	_High
	btfsc	MSB_Temp,7
	 bsf	_High,1
	btfsc	MSB_Temp,6
	 bsf	_High,0
	clrf	Temp
	movlw	4
	movwf	Count

Convert_TempLp
	SUB16  _Low,Count

	incf	BuffCount,F
	goto	Convert_TempLp
	retlw	0
