; ***************************************************************************
; dly_Hi = ((Fhz * Tsec)\3072) + 2
; Time Delay = ((dly_Hi + 770) * 4 * 7)\Fhz
; ---------------------------------------------------------------------------
; If dly_hi = 1 then
; dly_lo =  1 + ((Tsec(Fhz/4)) - 7) / 3
; Time Delay = 3(dly_lo - 1) + 7
; 
; ---------------------------------------------------------------------------
;
;4cycles ; setup to call
;2cycles ; call Delay
;(3(dly_Lo-1) + 2 + 3) = 1st cycle
;(255*3+2) + 3 = subsiquant cycles while dly_Hi > 1
;(255*3+2) + 2 = last cycle
;2cycles ; return from call
;
;sup call [     1st cycle       ]   [ additional sycles ]     [ last cycle   ]    return
;4 +  2 +  (3(dly_Lo-1) + 2 + 3)  +    ((255*3+2) + 3)      +  (255*3+2) + 2)  +    2
;4 +  2 +  (3(dly_Lo-1) + 2 + 3)  +         770             +     769          +    2
;
;
;          lst cycle               additional cyc   last cyc
;4 + 2 + (3(dly_Lo-1) + 2 + 3) +   770(dly_Hi-1)    + 769     + 2
;        (3(dly_Lo-1) + 2 + 3) +   770(dly_Hi-1)    + 769     + 8
;
;  1st cycle is from 5 - 770 cycles
; delay values from 15cycles on up if both regs are 1
; for a 1us inctruction time +1ms 1st cycle + lastcycle + 8
;                            +2ms 1stcycle + 1additional cycle + last cycle + 8 
;
;
;
Dlay5ms	movlw	35		; Delay 1ms
        movwf	dly_Hi
		movlw	123
        movwf	dly_Lo
		goto	Delay

Dlay20	movlw	1		; Delay 20us
        movwf   dly_Hi
        movlw   30
        movwf   dly_Lo
		goto	Delay

Dlay2	movlw	1		; Delay 20us
        movwf   dly_Hi
        movlw   1
        movwf   dly_Lo	
Delay				; Delay
  ifndef DEBUG
	decfsz  dly_Lo,F
     goto  	Delay
  	decfsz 	dly_Hi,F
     goto	Delay      
  endif
	retlw	0

