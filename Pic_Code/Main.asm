;***********************************************************
;
; This Program is for a n Alarm clock based on
; a DS3231 RTC chip, with internal crystal osc.
;
; I hope to use a 122x32 graphic display with
; green backlight.
;
; The char Module will be used for debugging.
;
;***********************************************************
;

DEBUG

  LIST P=16F59, R=DEC
  include P16F5X.INC
  __CONFIG _CP_OFF & _WDT_OFF & _HS_OSC

RTC_PORT equ PORTE
RTC_ALARM equ 4
I2c_PORT equ PORTE
I2c_SDA_In	equ b'10000000'	; Default State, with SDA=input
I2c_SDA_Out	equ b'00000000'

;**************************************************
; Register Address Defines
;**************************************************
;bank0	16-31	 ; I2c
;bank1  48-63
;bank2	80-95
;bank3	112-127
;bank4	144-159
;bank5	176-191
;bank6	208-223
;bank7	240-255

;These Regs are accessible anywhere!!
 CBLOCK 10
_Status		;  Reset Value
_Low		; 16 bit Reg
_High
Count		; Loop reg.
Temp		; Temp reg.
BuffCount	; byte counter
 endc

FLAGS	equ _Status
dly_Lo	equ	_Low
dly_Hi	equ	_High

;Buff equ 16	; RTC_Time & alarm registers
;Buff16	equ	48	; RTC_Temp & offset
				; Keypad Registers
RoutineSel equ 58 ;used in rtcSet.inc
				; I2c = 59-63
SnoozeDly equ 63 ; bit 7 = snooze enabled
Buff0	equ	80  ; 80-95 Memory Buffer

;Buff32	equ 112	 ; Memory for alarm storage = 80 registers
 CBLOCK 112
;Day of Week Alarms-------
A0DOW		;01111111 bit7 = enable?
A0Mn
A0Hr
A1DOW
A1Mn
A1Hr
 endc
Buff48	equ 144
Buff64	equ 176
Buff80	equ 208
Buff96	equ 240


;--------------------------------------------------------------
Page0 macro
	bcf	STATUS,PA0
	bcf	STATUS,PA1
 endm

Page1 macro
	bsf	STATUS,PA0
	bcf	STATUS,PA1
 endm

Page2 macro
	bcf	STATUS,PA0
	bsf	STATUS,PA1
 endm

Page3 macro
	bsf	STATUS,PA0
	bsf	STATUS,PA1
 endm

;------------------------- Program Entry -----------------------
 org 0
	btfsc	STATUS,NOT_TO	; did wdt reset or wakeup?
	 goto	TO1
	btfsc	STATUS,NOT_PD
	 goto	POR		; wdt_reset
	goto	Ramclr		; wdt_wakeup
TO1	btfss	STATUS,NOT_PD
	 goto	Ramclr		; wakeup from sleep

POR
	movlw	b'00001111'	; Setup the Port Registers
   	movwf	PORTA
    movlw	b'00001111'	; Setup PORTA
	TRIS	PORTA

	movlw	b'11111111'	; Control
	movwf	PORTB
    movlw	b'11111111'
	TRIS	PORTB

	movlw	b'11111111'		; Setup PORTC
    movwf	PORTC			; is broken??
	movlw	b'00111111'
	TRIS	PORTC

	movlw	b'00000000'	; All outputs Low
    movwf	PORTD
    movlw	b'00000000'	; Setup PORTD
	TRIS	PORTD

	movlw	b'11100000'	; turn alarm light on, bit 4=0
    movwf	PORTE
    movlw	I2c_SDA_Out	; Setup PORTE
	movlw	b'00000000'
	TRIS	PORTE

    movlw   b'00101111'     ; Timer0 inc on TOCKI, bit5, 
	OPTION					; WDT = 1:128 prescale


Ramclr
	movf	STATUS,W
	movwf	_Status
	movlw	H'FF'	; Stop at MEM location
	movwf	Count
	movlw	H'00'	; Clear all locations
	movwf	Temp
	movlw	H'10'	; Start with MEM Location 16
	call	ClearFSR

intsetp					; Peripherial Setup
	movf	STATUS,W	; only init when power up reset
	andlw	b'00011000'
	xorlw	b'00011000'
	btfss	STATUS,Z
	 goto	intset1
	bsf		STATUS,PA0		; Setup LCD
	call	LCDInit
	bcf		STATUS,PA0

intset1
	movlw	_Sec		; this makes to display time
	movwf	FSR
	movlw	255
	movwf	_Sec

	movlw	PSwitches	; Time Button = Set Default values to local registers
	movwf	FSR
	movlw	2			; Setup Key Repeate Rate
	movwf	SW_RepeateDly
;	movlw	1
;	movwf	_PSwitches
;	movwf	_RSwitches

;	Page3
;	call	SetDefault
	Page2				; Get Time Registers
	call	ReadDS3231	; Check OSF
	Page0
	movlw	Sec
	movwf	FSR
	btfss	RTC_Stat,OSF
	 goto	MainLoop

;------ if clock was stopped then Set Time ----------------
; that either power failure, or just plugged in, either way time needs to be set
ResetTime
	movlw	Hour
	movwf	FSR
	movlw	b'01010010'	; Set to 12: am
	movwf	Hour
	movlw	8
	movwf	Year
	movlw	3
	movwf	Day
	movlw	1
	movwf	Date
	movlw	1
	movwf	Month
	movlw	b'00001000'		; Clear Flags enable OSC
	movwf	RTC_Stat
	movlw	b'00110100'		; 4khz pin enabled, no bat OSC, Alarm Disabled, Convert Temp
	movwf	RTC_Ctl
	Page2
	call	WriteTime		; Update The Time
	call	WriteStatCtrl	; Update Status and Control
	Page3				; Now Set The Time, Assuming since osc was stopped
	goto	KP_SetTime
	Page0
	goto	MainLoop


;------------------------- Setup Complete -----------------------
 include	FSR.inc
 include	beep.inc
 include	keypad.inc ; 6 button keypad

;***********************************************************
; Program Start
;***********************************************************
;

MainLoop
	nop
  ifndef DEBUG
	Page2
	call	ReadDS3231
	Page0
	call	keypad
  endif

;---------- Set Time ??????? -----------------------------------------
Test_SetTime
	movlw	PSwitches
	movwf	FSR
	movf	PSwitches,W
	xorlw	b'01000100'		; Sleep+Time = Set Time = bits 2 and 6
	btfss	STATUS,Z
	 goto	CheckAlarm_Bits
	Page3
	goto	KP_SetTime
SetTime_Done

;---------------------------------------------------------------------
;Alarm Needs update ????????
; update Day of week when Time 12:00 am
CheckAlarm_Bits
	movlw	Sec
	movwf	FSR
	movf	Hour,W		; hour = 12
	xorlw	b'01010010'
	btfss	STATUS,Z
	 goto	skipDOY
	movf	Min,W
	xorlw	0
	btfss	STATUS,Z
	 goto	skipDOY
	Page3
	goto	AlarmWriteTest	; test the Registers for Day of Week
							; this branches
skipDOY

;---------- Set Alarm ??????? -----------------------------------------
Test_SetAlarm
	movlw	PSwitches
	movwf	FSR
	movf	PSwitches,W
	xorlw	b'00001100'		; Sleep+Hour = Set Alarm1 = bits 2 and 3
	btfsc	STATUS,Z
	 goto	SetAlarm1
	movf	PSwitches,W
	xorlw	b'00010100'		; Sleep+Min = Set Alarm0 = bits 2 and 4
	btfss	STATUS,Z
	 goto	M_DisplayTime

SetAlarm0
	Page3
	goto	KP_Alarm0
SetAlarm1
	Page3
	goto	KP_Alarm1

WriteA1
	movf	Temp,W
	xorlw	1
	btfsc	STATUS,Z
	 goto	WriteA2
	movf	A0Mn,W
	movwf	_Low
	movf	A0Hr,W
	movwf	_High
	movlw	A1Min
	movwf	FSR
	movf	_Low,W
	movwf	A1Min
	movf	_High,W
	movwf	A1Hour
	movf	BuffCount,W
	movwf	A1Date
	bsf		A1Date,6		; set day/date bit, Alarm on day not date
	bcf		RTC_Ctl,A2IE
	bsf		RTC_Ctl,A1IE	; no alarm1

	movlw	7			; write 8 bytes
	movwf	BuffCount	; Just Send Address, for Read
	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	A1Min		; Set Save Location
	movwf	I2Buff0
	movlw	H'08'		; write Alarm Address
	movwf	BuffAddr
	goto	AlarmWrite

WriteA2
	movf	Temp,W
	xorlw	2
	btfsc	STATUS,Z
	 goto	WriteA12
	movf	A1Mn,W
	movwf	_Low
	movf	A1Hr,W
	movwf	_High
	movlw	A2Min
	movwf	FSR
	movf	_Low,W
	movwf	A2Min
	movf	_High,W
	movwf	A2Hour
	movf	BuffCount,W
	movwf	A2Date
	bsf		A2Date,6	; set day/date bit, Alarm on day not date
	bsf		RTC_Ctl,A2IE
	bcf		RTC_Ctl,A1IE	; no alarm1

	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	4			; write 4 bytes
	movwf	BuffCount	; Just Send Address, for Read
	movlw	A2Min		; Set Save Location
	movwf	I2Buff0
	movlw	H'0B'		; write Alarm Address
	movwf	BuffAddr
	goto	AlarmWrite

WriteA12
	movf	Temp,W
	xorlw	3
	btfsc	STATUS,Z
	 goto	DisableAlarms
	movf	A0Mn,W
	movwf	_Low
	movf	A0Hr,W
	movwf	_High
	movlw	A1Min
	movwf	FSR
	movf	_Low,W
	movwf	A1Min
	movf	_High,W
	movwf	A1Hour
	movf	BuffCount,W
	movwf	A1Date
	bsf		A1Date,6		; set day/date bit, Alarm on day not date
	movlw	A0DOW
	movwf	FSR
	movf	A1Mn,W
	movwf	_Low
	movf	A1Hr,W
	movwf	_High
	movlw	A2Min
	movwf	FSR
	movf	_Low,W
	movwf	A2Min
	movf	_High,W
	movwf	A2Hour
	movf	BuffCount,W
	movwf	A2Date
	bsf		A2Date,6	; set day/date bit, Alarm on day not date
	bsf		RTC_Ctl,A2IE
	bsf		RTC_Ctl,A1IE

	movlw	7			; write 7 bytes
	movwf	BuffCount	; Just Send Address, for Read
	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	A1Min		; Set Save Location
	movwf	I2Buff0
	movlw	H'08'		; write Alarm Address
	movwf	BuffAddr
	goto	AlarmWrite

DisableAlarms
	movlw	RTC_Ctl
	movwf	FSR
	bcf		RTC_Ctl,A2IE	; Disable the interrupt signals
	bcf		RTC_Ctl,A1IE

	movlw	1			; write 1 bytes
	movwf	BuffCount
	movlw	I2Buff0		; set FSR = to MEM Page
	movwf	FSR
	movlw	RTC_Ctl		; Set Save Location
	movwf	I2Buff0
	movlw	H'0E'		; write Alarm Address
	movwf	BuffAddr

AlarmWrite
	Page2
	movlw	RTC_Addr
	call	I2cWrite
	Page0


;--------------------------------------------------------------------------
;Sound Alarm Tests the Alarm Flags in the Status Register and sounds alarm
AlarmSound
	movlw	RTC_Stat
	movwf	FSR
	btfsc	RTC_Stat,A1F
	 goto	Snooze_Test
	btfss	RTC_Stat,A2F
	 goto	M_DisplayTime

Snooze_Test
	movlw	PSwitches
	movwf	FSR
	btfsc	RSwitches,SW_Snooze		; if snooze pressed then wait for 5min
	 goto	Sound_Alarm				; before sounding alarm again
	movlw	5
	movwf	Count
IncMinSnooze				; add 5 to current Min
	Page3
	call	IncMin
	Page0
	decfsz	Count,F
	 goto	IncMinSnooze
	movf	Min,W
	movwf	_Low
	movlw	SnoozeDly		; Save Min+5 to SnoozeDly
	movwf	FSR
	movf	_Low,W
	movwf	SnoozeDly
	bsf		SnoozeDly,7		; enable snooze

Sound_Alarm
	movlw	SnoozeDly			; Delay 5 Min
	movwf	FSR
	movf	SnoozeDly,W			; Delay 5 Min
	movwf	_Low
	movlw	Min
	movwf	FSR
	movlw	128
	addwf	Min,W		; if b'1xxxxxxx' = SnoozeDly
	xorwf	_Low		; if equal then time is up
	btfsc	STATUS,Z
	 goto	Sound_AlarmDone
	movlw	SnoozeDly			; Delay 5 Min
	movwf	FSR
	bcf		SnoozeDly,7		; Disable Snooze Delay

	movlw	26		; 625hz			; Min Pressed?
	movwf	Temp
	movlw	248		; 50ms
	call	BEEP
Sound_AlarmDone


;---------- Update Display ??????? -----------------------------------------
M_DisplayTime
	movlw	Sec			; if regs are = don't display
	movwf	FSR
	movf	Sec,W
	movwf	_Low
	movlw	_Sec
	movwf	FSR
	movf	_Low,W
	xorwf	_Sec,W
	btfsc	STATUS,Z
	 goto	MainLoop
	Page1
	call	Line1		; 12:01.23pm
	call	DispHr
	call	DispMin
	call	DispSec
	call	Line2		; Mon Jul 22, 2008
	call	DispDay
	call	DispMonth
	call	DispDate
	call	DispYear
;	Page2				; Display Temperature-----------------
;	call	Convert_TempC	; needs testing
	movlw	11
	call	LcdAd
	call	DispTemp

;	movlw	15		; Display Alarm Indicator   ----------------------
;	call	LcdAd
	Page0
	movlw	RTC_Ctl		;Display Alarm Indicator
	movwf	FSR			; 0=noalarm, 1=alarm1, 1 = alarm2, 3 = both
	movlw	1
	movwf	Temp
	btfsc	RTC_Ctl,A1IE
	 addwf	Temp,F
	btfsc	RTC_Ctl,A2IE
	 addwf	Temp,F
	btfsc	RTC_Ctl,A2IE
	 addwf	Temp,F
	decf	Temp,W
	xorlw	0
	btfss	STATUS,Z	; if 0 then display space
	 goto	$+4			; else display alarm char
	movlw	' '
	goto	$+2
	decf	Temp,W
	Page1
	call	LCDChar
	Page0
	movlw	Sec			; Backup Sec Reg
	movwf	FSR
	movf	Sec,W
	movwf	_Low
	movlw	_Sec
	movwf	FSR
	movf	_Low,W
	movwf	_Sec
	goto	MainLoop


;---------------------- Testing --------------------------------------------
;TestChars
;	Page1
;	movlw	11
;	call	LcdAd
;	movf	_Status,W
;	call	LCDChar
;	movf	_Status,W
;	call	DispHex
;	movlw	PSwitches	; Time Button = Set Default values to local registers
;	movwf	FSR
;	btfsc	RSwitches,SW_Min	;min = inc
;	 incf	_Status,F
;	Page0
;	goto	MainLoop

;------------------------------------------------
 org H'200'
 include	rtcDisp.inc
 include	b_conv.inc
 include	c550001.inc	 ; 16x2 char module
 include	CustomChar.inc


 org H'400'
; include	x1243.inc
 include	DS3231.inc
 include	I2c.inc

 org H'600'
 include	rtcSet.inc
 include	SetTime.inc
 include	SetAlarm.inc
 end
