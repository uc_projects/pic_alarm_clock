KeypadLoop
;----------------------------------------------------------------------------------
	btfss	RSwitches,2		; Wait for Alarm press, to Set Default Local Regs
	 goto	Write_Default
	bsf		STATUS,PA0		; Write Default values to Local RTC Regs
	bsf		STATUS,PA1
	call	SetDefault
	bcf		STATUS,PA0
	bcf		STATUS,PA1
	goto	Update_Disp+2
Write_Default   ; if wait for Sleep press, write Sec, Min, Hour, Day, Date, Year
				;    To RTC
	btfss	RSwitches,6		; goto Next if no pressed
	 goto	Read_Rtc
	bsf		STATUS,PA1
;	call	W_DefaultRtc	
	bcf		STATUS,PA1
	goto	$+3
Read_Rtc	;  if wait for Time Pressed, Read Time, Date, control and status
	btfss	PSwitches,3		; goto Next if no pressed
	 goto	Update_Disp
	bsf		STATUS,PA1		; Copy RTC to Local Registers
;	call	ReadDS3231
	bcf		STATUS,PA1
	goto	Update_Disp+2

Update_Disp  ; Wait for snooze Press -------------------
	btfss	PSwitches,7		; Loop if no Snooze
	 goto	Main
	goto	Display_Page0
