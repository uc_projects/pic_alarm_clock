;***********************************************************
;
; This Program is for a n Alarm clock based on
; a DS3231 RTC chip, with internal crystal osc.
;
; I hope to use a 122x32 graphic display with
; green backlight.
;
; The char Module will be used for debugging.
;
;***********************************************************
;

;DEBUG

  LIST P=16F59, R=DEC
  include P16F5X.INC
  __CONFIG _CP_OFF & _WDT_OFF & _HS_OSC

RTC_PORT equ PORTE
RTC_ALARM equ 4
I2c_PORT equ PORTE
I2c_SDA_In	equ b'10000000'	; Default State, with SDA=input
I2c_SDA_Out	equ b'00000000'

;**************************************************
; Register Address Defines
;**************************************************
;bank0	16-31	 ; I2c
;bank1  48-63
;bank2	80-95
;bank3	112-127
;bank4	144-159
;bank5	176-191
;bank6	208-223
;bank7	240-255

;These Regs are accessible anywhere!!
 CBLOCK 10
_Status		;  Reset Value
_Low		; 16 bit Reg
_High
Count		; Loop reg.
Temp		; Temp reg.
BuffCount	; byte counter
 endc

;Buff equ 16	;16-31
;------------- Paged Addressing starts here--------------------
;Buff16	equ	48	  ;48 used to debounce switch
RoutineSel equ 63 ;used in rtcSet.inc

Buff0	equ	80    ; 80-95  LCD Memory
;Buff32	equ 112	; RTC
;Buff48	equ 144 ; RTC

Buff64	equ 176
Buff80	equ 208
Buff96	equ 240

;------------------------- Program Entry -----------------------
 org 0
	btfsc	STATUS,NOT_TO	; did wdt reset or wakeup?
	 goto	TO1
	btfsc	STATUS,NOT_PD
	 goto	POR		; wdt_reset
	goto	Ramclr		; wdt_wakeup
TO1	btfss	STATUS,NOT_PD
	 goto	Ramclr		; wakeup from sleep

POR
	movlw	b'00000000'	; Setup the Port Registers
   	movwf	PORTA
    movlw	b'00001110'	; Setup PORTA
	TRIS	PORTA

	movlw	b'11000011'	; Control
;	movlw	b'11111111'
	movwf	PORTB
    movlw	b'11000011'
	TRIS	PORTB

	movlw	b'11111111'		; Setup PORTC
    movwf	PORTC			; is broken??
	movlw	b'00111111'
	TRIS	PORTC

	movlw	b'00000000'	; All outputs Low
    movwf	PORTD
    movlw	b'00000000'	; Setup PORTD
	TRIS	PORTD

	movlw	b'11100000'	; turn alarm light on, bit 4=0
    movwf	PORTE
    movlw	I2c_SDA_Out	; Setup PORTE
	movlw	b'00000000'
	TRIS	PORTE

    movlw   b'00101111'     ; Timer0 inc on TOCKI, bit5, 
	OPTION					; WDT = 1:128 prescale


Ramclr
	movf	STATUS,W
	movwf	_Status
	movlw	H'FF'	; Stop at MEM location
	movwf	Count
	movlw	H'00'	; Clear all locations
	movwf	Temp
	movlw	H'10'	; Start with MEM Location 16
	call	ClearFSR

intsetp					; Peripherial Setup
	movf	STATUS,W	; only init when power up reset
	andlw	b'00011000'
	xorlw	b'00011000'
	btfss	STATUS,Z
	 goto	intset1
	bsf		STATUS,PA0		; Setup LCD
	call	LCDInit
	bcf		STATUS,PA0

intset1
	goto	Main

;------------------------- Setup Complete -----------------------
 include	FSR.inc
 include	beep.inc
 include	keypad.inc ; 6 button keypad

;***********************************************************
; Program Start
;***********************************************************

;I found that the RTC is giving erratic results when reading,
;that problem was coupled with a display problem that was fixed
;with proper lcdinit routine.
;
;I made routine that read RTC on startup then
; updated the display from local registers on keypress,7
; the result was that the numbers were the same. so it's not changing
; with display code.!
;
;I'll try to write to the registers on keypress,6 with a static value
; if the registers hold then writing is sucessful, and i can analyze the
; RTC through the read routine by holding key,3. and just refresh by holding
; key,7 which will refrech the display with local time regs
;
;
Main; 50ms
	movlw	PSwitches	; Time Button = Set Default values to local registers
	movwf	FSR
	movlw	1
	movwf	_PSwitches
	movwf	_RSwitches
	movlw	2
	movwf	SW_RepeateDly

MainLoop
	call	keypad
	movf	PSwitches,W		; Display differance if switch set
	xorwf	_PSwitches,W
	btfsc	STATUS,Z
	 goto	$+6
	bsf		STATUS,PA0
	call	Line1
	movf	PSwitches,W
	call	DispBin
	bcf		STATUS,PA0

	movf	RSwitches,W
	xorwf	_RSwitches,W
	btfsc	STATUS,Z
	 goto	$+6
	bsf		STATUS,PA0
	call	Line2
	movf	RSwitches,W
	call	DispBin
	bcf		STATUS,PA0

	btfss	PSwitches,SW_Time
	 goto	$+5
	movlw	24		; 675hz
	movwf	Temp
	movlw	34		; 50ms
	call	BEEP

	btfss	PSwitches,SW_Alarm
	 goto	$+5
	movlw	26		; 625hz
	movwf	Temp
	movlw	31		; 50ms
	call	BEEP

	btfss	PSwitches,SW_Min
	 goto	$+5
	movlw	35		; 475hz
	movwf	Temp
	movlw	24		; 50ms
	call	BEEP

	btfss	PSwitches,SW_Hour
	 goto	$+5
	movlw	73		; 225hz
	movwf	Temp
	movlw	11		; 50ms
	call	BEEP

	btfss	PSwitches,SW_Sleep
	 goto	$+5
	movlw	94		; 175hz
	movwf	Temp
	movlw	9		; 50ms
	call	BEEP

	btfss	PSwitches,SW_Snooze
	 goto	$+5
	movlw	110		; 125hz
	movwf	Temp
	movlw	7		; 50ms
	call	BEEP

	goto	MainLoop
; include	keypadLoop.inc

;------------------------------------------------
 org H'200'
 include	c550001.inc	 ; 16x2 char module
; include	32122a.inc	 ; 122x32 graphic module
 include	b_conv.inc
; include	DispPage.inc

 org H'400'
; include	x1243.inc
; include	I2c.inc
; include	DS3231.inc

 org H'600'
; include	rtcSet.inc
; include font.inc	; This Last line of code	
 end
