
SetDefault
	movlw	Min			; Setup The Registers
	movwf	FSR
	movlw	b'01011001'	; 59min
	movwf	INDF
	incf	FSR,F
	movlw	b'01101001' ; 9pm	;Hour
;	movlw	b'00000000' ; 0900  ;24hour
	movwf	INDF
	incf	FSR,F	;	movlw	Day
	movlw	6
	movwf	INDF
	incf	FSR,F	;	movlw	Date
	movlw	4
	movwf	INDF
	incf	FSR,F	;	movlw	Month
	movlw	7
	movwf	INDF
	incf	FSR,F	;	movlw	Year
	movlw	8
	movwf	INDF
	incf	FSR,F	;	movlw	A1Sec
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	A1Min
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	A1Hour
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	A1Date
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	A2Min
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	A2Hour
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	A2Date
	movlw	0
	movwf	INDF
	incf	FSR,F	;	movlw	RTC_Ctl
	movlw	b'00000000'
	movwf	INDF
	incf	FSR,F	;	movlw	RTC_Stat
	movlw	b'00000000'
	movwf	INDF
	retlw	0
