;---------------------------------------------------------------------------
; 3wire Serial
;---------------------------------------------------------------------------
;
; Pins and Ports
S3WPort equ PORTC
S3WCs	equ 4	 			; Chip Select
S3WDi	equ	1 				; Data Input, used to clock in a Start bit, opcode,
 							;  address and data synchronously with the CLK input
S3WDo	equ 0				; Data Output, This pin input always
S3Wclk	equ 6				; Serial Clock
; Opcodes, address and data bits are clocked in on the positive edge of CLK.
;Data bits are also clocked out on the positive edge of CLK. CLK can be stopped 
;anywhere in the transmission sequence (at high or low level) and can be continued
;anytime with respect to Clock High Time (TCKH) and Clock Low Time (TCKL). This
;gives the controlling master freedom in preparing opcode, address and data.
; 
S3WcsdiHi equ b'00010010'	; Set CS and DI, other pins are defaults???
S3WcsdiLo equ b'00000000'	; Clear CS and DI, other pins are defaults???
 
; 3Worg		  ; Select 8 or 16 bit organization, This Code is for 16bit
; When the ORG pin is connected to VCC or Logic HI, the(x16) memory organization
;is selected. When the ORG pin is tied to VSS or Logic LO, the (x8) memory
;organization is selected. For proper operation, ORG must be tied to a valid logic level
;
; 		 CS DI Do CLK ORG
; IDLE =  0  0  I   0   x
; z = high impeadance
; I = input
; 0 = logic low output
;


;-------------------------------------------------------
; Registers, Do not change Register Order
 CBLOCK 176
BitCount	 	; Counter for bits
;BuffCount		; for byte Count
S3Wopcode
S3WBuffAddr  	; eeprom address storage
;S3WBuffAddrHi
S3WDataHi		; Data Storage registers
S3WDataLo
;dly_Lo			; Soft Dly regs.
;dly_Hi
 endc



;----------------- Timing Macros -----------------------------
; The Timing is largly dependant on the supply voltage,
;Higher voltage = faster timing, the times below are for 3.3v
;on a 20mhz pic

;Clock High Time--------------------
Tckh macro
	 goto  $+1	  ; 250ns
 endm

;Clock Low Time 
Tckl macro
	 nop   		  ; 200ns 
 endm 

; Chip Select Setup Time -----------
Tcss macro
	 nop  	  	  ; 100ns
 endm
 
; Chip Select hold Time
Tcsh macro
	 	  	  	  ; 0ns
 endm 
 
; Chip Select low Time
Tcsl macro
	 goto  $+1	  ; 250ns
 endm
 
; Data input setup time -------------
Tdis macro
	 nop  	  	  ; 100ns
 endm
 
; Data input hold time
Tdih macro
	 nop  	  	  ; 100ns
 endm

; Data Output delay time -------------
Tpd macro
	 goto  $+1	  ; 250ns
 endm   
 
; Data output Disable Time
Tcz macro
	 nop   		  ; 200ns 
 endm
 
; Status valid Time
Tsv macro
	 goto  $+1	  ; 300ns
	 nop	 
 endm
 
; Program cycle times --------------------------------
; these are the 4.5v values may need more delay
Twc macro
	movlw 	5	  	  ; 6ms
	movwf	dly_Hi
	clrf	dly_Lo
	decfsz  dly_Lo,F
	 goto	$-1
	decfsz	dly_Hi,F
	 goto	$-3
 endm
 
Tec macro
	movlw 	5	  	  ; 6ms
	movwf	dly_Hi
	clrf	dly_Lo
	decfsz  dly_Lo,F
	 goto	$-1
	decfsz	dly_Hi,F
	 goto	$-3
 endm    

Twl macro
	movlw 	5	  	  ; 15ms
	movwf	dly_Hi
	clrf	dly_Lo
	decfsz  dly_Lo,F
	 goto	$-1
	decfsz	dly_Hi,F
	 goto	$-3
 endm
 
 
 
 
;--------------------- Macros ----------------------------------------- 
; The Start bit is detected by the device if CS and DI are
;both high with respect to the positive edge of CLK for
;the first time.
;
; Before a Start condition is detected, CS, CLK and DI
;may change in any combination (except to that of a
;Start condition), without resulting in any device
;operation (Read, Write, Erase, EWEN, EWDS, ERAL
;or WRAL). As soon as CS is high, the device is no
;longer in Standby mode.
;
; An instruction following a Start condition will only be
;executed if the required opcode, address and data bits
;for any particular instruction are clocked in.
;
; When preparing to transmit an instruction, Block Diagram
; either the CLK or DI signal levels must be
; at a logic low as CS is toggled active high
;
;If CS is high, but the Start condition has not been
;detected (DI = 0), any number of clock cycles can be
;received by the device without changing its status (i.e.,
;waiting for a Start condition).
;
S3W_START macro
	bcf		S3WPort,S3WCs		 ; Clear Clock
	movlw	S3WcsdiHi		 ; Set CS and DI, other pins are defaults???
	movwf	S3WPort
 endm		

S3W_STOP macro	 
	 movlw		 S3WcsdiLo
;	Tcsh
	 movwf		 S3WPort
;	Tcz				   		; Data output Disable Time	
 endm

S3W_Clk macro
; Opcodes, address and data bits are clocked in on the positive edge of CLK.
;Data bits are also clocked out on the positive edge of CLK
	bsf	S3WPort,S3Wclk
	Tckh					 ; Clock High Time
	bcf		S3WPort,S3Wclk
	Tckl  					 ; Clock Low Time
 endm		

; OPcodes with no Data Routine -----------------------------------------
S3SnData
 	S3W_START  				 ; set CS and DI high, and wait
 	Tcss		  			 ; Chip Select Setup Time
S3WnDopLp	
	S3W_Clk					 ;Clock the first bit

	decf	BitCount,F
	movf	BitCount,W
	xorlw	8
	btfsc	STATUS,Z
	 goto	S3WnDLp
	
	bcf	    S3WPort,S3WDi		 ; Clock Opcode
	btfsc   S3Wopcode,7
	 bsf    S3WPort,S3WDi
	rlf		S3Wopcode,F
	goto	S3WnDopLp

S3WnDLp	
	bcf	   S3WPort,S3WDi 		 ; Send Address
	btfsc  S3WBuffAddr,7
	 bsf   S3WPort,S3WDi

	S3W_Clk	  
	 rlf 	   S3WBuffAddr,F
	 decfsz	   BitCount,F
	  goto	   S3WnDLp
	 rlf 	   S3WBuffAddr,F	 ; restore File
 	S3W_STOP 	 	  		 ; Clear CS and DI wait Tsz and return
;	goto	S3WBusy
	retlw	0
 
 
;++++++++++++++++++++++ Support Functions +++++++++++++++++++++++++++++++++
;
; 
;Check Busy  -------------------------------------------------------------------:>
; This should Delay for Twc or about 5ms after a write
S3WBusy
	Tcsl	 				; Chip Select low Time
	Tsv						; Status valid Time
	bsf	    S3WPort,S3WCs
	btfss	S3WPort,S3WDo		; Clear = busy
	 goto	$-1				; wait for clear!!!!!
	bcf	    S3WPort,S3WCs
	Tcz				   		; Data output Disable Time
	retlw	0
	
	
;Verify Write -------------------------------------------------------------------
; Read Data after write and report error if mismatch
S3WVerifyW
	retlw	0
	retlw	1	  ; return error
	
	
;++++++++++++++++++++++ Standard Functions +++++++++++++++++++++++++++++++++
;
;
;ERAL  00 ----------------------------------------------------------
; The Erase All (ERAL) instruction will erase the entire
;memory array to the logical '1' state. The ERAL cycle
;is identical to the erase cycle, except for the different
;opcode. The ERAL cycle is completely self-timed and
;commences at the falling edge of the CS, except on
;93C devices where the rising edge of CLK before the
;last data bit initiates the write cycle. Clocking of the
;CLK pin is not necessary after the device has entered
;the ERAL cycle.
;
; The DO pin indicates the Ready/Busy status of the
;device, if CS is brought high after a minimum of 250 ns
;low (TCSL).
;
; Note: Issuing a Start bit and then taking CS low
; will clear the Ready/Busy status from DO.
;
;VCC must be . 4.5V for proper operation of ERAL.
;
;SB Opcode Address          Data_In  Data_Out  Req._CLK_Cycles
; 1   00   1 0 X X X X X X    -�     (RDY/BSY)    11
;
S3WERAL
	movlw	11
	movwf	BitCount
	movlw	b'00000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
	movlw	b'10000000'
	movwf	S3WBuffAddr
	goto	S3SnData


;Erase 11 ----------------------------------------------------------
; The ERASE instruction forces all data bits of the specified
;address to the logical �1� state. CS is brought low
;following the loading of the last address bit. This falling
;edge of the CS pin initiates the self-timed programming
;cycle, except on �93C� devices where the rising
;edge of CLK before the last address bit initiates the
;write cycle.
;
; The DO pin indicates the Ready/Busy status of the
;device if CS is brought high after a minimum of 250 ns
;low (TCSL). DO at logical �0� indicates that programming
;is still in progress. DO at logical �1� indicates that the
;register at the specified address has been erased and
;the device is ready for another instruction.
;
; Note: Issuing a Start bit and then taking CS low
; will clear the Ready/Busy status from DO.

;SB Opcode Address Data_In  Data_Out  Req._CLK_Cycles
; 1   11    A7-A0    �--    (RDY/BSY)   11
S3WErase
	movlw	11
	movwf	BitCount
	movlw	b'11000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
;	movlw	b'10000000'
;	movwf	S3WBuffAddr
	goto	S3SnData


;-------------------------------------------------------------------	
; The 93XX66A/B/C powers up in the Erase/Write
;Disable (EWDS) state. All Programming modes must be
;preceded by an Erase/Write Enable (EWEN) instruction.
;Once the EWEN instruction is executed, programming
;remains enabled until an EWDS instruction is executed
;or Vcc is removed from the device.
;
; To protect against accidental data disturbance, the
;EWDS instruction can be used to disable all erase/write
;functions and should follow all programming operations.
;Execution of a READ instruction is independent of
;both the EWEN and EWDS instructions.
;
; Erase/Write Disable------------------------------------------------
;
;SB Opcode Address          Data_In  Data_Out  Req._CLK_Cycles
;1     00  0 0 X X X X X X    -�      High-Z       11
;

S3WEWDS
	movlw	11
	movwf	BitCount
	movlw	b'00000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
	movlw	b'00000000'
	movwf	S3WBuffAddr	
	retlw	0

	
; Erase/Write Enable --------------------------------------------------
;
;SB Opcode Address          Data_In  Data_Out  Req._CLK_Cycles
;1    00    1 1 X X X X X X   -�      High-Z         11
;
;
S3WEWEN
	movlw	11
	movwf	BitCount
	movlw	b'00000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
	movlw	b'11000000'
	movwf	S3WBuffAddr	
	retlw	0

	
;Write 01 ----------------------------------------------------------
; The WRITE instruction is followed by 8 bits (If ORG is
;low or A-version devices) or 16 bits (If ORG pin is high
;or B-version devices) of data which are written into the
;specified address. For 93AA66A/B/C and 93LC66A/B/C
;devices, after the last data bit is clocked into DI, the
;falling edge of CS initiates the self-timed auto-erase and
;programming cycle. For 93C66A/B/C devices, the selftimed
;auto-erase and programming cycle is initiated by
;the rising edge of CLK on the last data bit.
;
; The DO pin indicates the Ready/Busy status of the
;device, if CS is brought high after a minimum of 250 ns
;low (TCSL). DO at logical �0� indicates that programming
;is still in progress. DO at logical �1� indicates that the
;register at the specified address has been written with
;the data specified and the device is ready for another
;instruction.
;
; Note: Issuing a Start bit and then taking CS low
; will clear the Ready/Busy status from DO.
;
; Write 16bits to memory location
;
;SB Opcode Address                     Data_In    Data_Out  Req._CLK_Cycles
;1    01   A7 A6 A5 A4 A3 A2 A1 A0     D15 � D0   (RDY/BSY)      27
;
S3WWrite
	movlw	S3Wopcode
	movwf	FSR
	movlw	3			; Send (BitCount-1) Bits
	movwf	BitCount
	movlw	4
	movwf	BuffCount	; Send (BuffCount-1) Bits
	movlw	b'01000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
;	movlw	b'11000000'
;	movwf	S3WBuffAddr	
	nop
 	S3W_START  				 ; set CS and DI high, and wait
 	Tcss		  			 ; Chip Select Setup Time
S3WWriteopLp	
	S3W_Clk					 ;Clock the first bit

	bcf	    S3WPort,S3WDi		 ; Clock Bit
	btfsc   INDF,7
	 bsf    S3WPort,S3WDi
	rlf		INDF,F

	decfsz	BitCount,F
	 goto	S3WWriteopLp
	movlw	8
	movwf	BitCount
	incf	FSR
	decfsz	BuffCount,F
	 goto	S3WWriteopLp

 	S3W_STOP 	 	  		 ; Clear CS and DI wait Tsz and return
;	goto	S3WBusy
	retlw	0	
	
	
;WRAL  00 ----------------------------------------------------------
;
;SB Opcode Address                     Data_In    Data_Out   Req._CLK_Cycles
;1    00    0 1 X X X X X X            D15 � D0    (RDY/BSY)     27
;
S3WWRAL
	movlw	S3Wopcode
	movwf	FSR
	movlw	3			; Send (BitCount-1) Bits
	movwf	BitCount
	movlw	4
	movwf	BuffCount	; Send (BuffCount-1) Bits
	movlw	b'00000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
	movlw	b'01000000'
	movwf	S3WBuffAddr	
	goto	S3WWrite+8
	
	
	
	
;Read  10 ---------------------------------------------------------------------
; The output data bits will toggle on the rising edge of the CLK and are stable 
;after the specified time delay (TPD). Sequential read is possible when CS is
;held high. The memory data will automatically cycle to the next register and
;output sequentially.
;
;A dummy zero bit precedes the output string
;
;SB Opcode Address                     Data_In    Data_Out   Req._CLK_Cycles
; 1    10   A7 A6 A5 A4 A3 A2 A1 A0       �       D15 � D0        27
;
;
S3WRead
	movlw	S3Wopcode
	movwf	FSR
	movlw	3			; Send (BitCount-1) Bits
	movwf	BitCount
	movlw	2
	movwf	BuffCount	; Send (BuffCount-1) Bits
	movlw	b'10000000'		; bit 7,6 = opcode
	movwf	S3Wopcode
;	movlw	b'11000000'
;	movwf	S3WBuffAddr	

 	S3W_START  				 ; set CS and DI high, and wait
 	Tcss		  			 ; Chip Select Setup Time
S3WReadopLp	
	S3W_Clk					 ;Clock the first bit

	bcf	    S3WPort,S3WDi		 ; Clock Bit
	btfsc   INDF,7
	 bsf    S3WPort,S3WDi
	rlf		INDF,F

	decfsz	BitCount,F
	 goto	S3WReadopLp
	movlw	8
	movwf	BitCount
	incf	FSR
	decfsz	BuffCount,F
	 goto	S3WReadopLp
	decf	FSR				;roll back FSR

;-------------- Read Data -------------------------------
	movlw	2
	movwf	BuffCount	; Send (BuffCount-1) Bits
S3WReadLp
	movlw	8
	movwf	BitCount
	incf	FSR
	clrf	INDF
S3WReadLp1
	Tpd
	S3W_Clk					 ; Do Stable on Low Clock

	bcf		INDF,0 		 	 ; Send Address
	btfsc	S3WPort,S3WDo
	 bsf 	INDF,0
	rlf		INDF,F

	decfsz	BitCount,F
	 goto	S3WReadLp1
	decfsz	BuffCount,F
	 goto	S3WReadLp

 	S3W_STOP 	 	  		 ; Clear CS and DI wait Tsz and return
;	goto	S3WBusy
	retlw	0
