Display_Page0
; Read Local RTC Registers and Display Data in Hex ----------
;Line1: Sec Min  Hour  RTC_Ctl RTC_Stat = 14 chars
;Line2: Day Date Month Year             = 11 chars

	bsf		STATUS,PA0
	call	Line1
	movlw	Sec			; Secound
	movwf	FSR
	movf	Sec,W
	call	DispHex
	call	LCDspac
	movlw	Min			; Minuet
	movwf	FSR
	movf	Min,W
	call	DispHex
	call	LCDspac
	movlw	Hour		; Hour
	movwf	FSR
	movf	Hour,W
	call	DispHex
	call	LCDspac
	movlw	RTC_Ctl		; RTC_Ctl
	movwf	FSR
	movf	RTC_Ctl,W
	call	DispHex
	call	LCDspac
	movlw	RTC_Stat	; RTC_Stat
	movwf	FSR
	movf	RTC_Stat,W
	call	DispHex

	call	Line2
	movlw	Day			; Day
	movwf	FSR
	movf	Day,W
	call	DispHex
	call	LCDspac
	movlw	Date		; Date
	movwf	FSR
	movf	Date,W
	call	DispHex
	call	LCDspac
	movlw	Month		; Month
	movwf	FSR
	movf	Month,W
	call	DispHex
	call	LCDspac
	movlw	Year		; Year
	movwf	FSR
	movf	Year,W
	call	DispHex
	call	LCDspac		;Display Temp
	movlw	MSB_Temp
	movwf	FSR
	movf	MSB_Temp,W
	call	DispHex
	movlw	LSB_Temp
	movwf	FSR
	movf	LSB_Temp,W
	call	DispHex
	bcf		STATUS,PA0
	goto	Main
