Beep_Port equ PORTE	; Port
Beep_Pin equ 4		; Port Pin
;
;
;Setup -------------------------------------------------
; Timer0= TOCKI Pin = 32Khz, no prescaler, Low to High
; TMR0 increments every 1/fTOCKI = 1/32.768khz = 30.5175us = tTMR0Increment
; Timer OverFlows every 256*tTMR0Increment = 0.0078125 = 7.8ms = tTMR0_OverFlow
;
;  Max Length = tTMR0_OverFlow*255 = 1.9921875Sec
;
; Min Freq = 1/( 256(256*(1/32768)) ) = 128hz
; Max Freq = 1/( 1(1*(1/32768)) ) = 32.768khz???
; Freq = ((1/Fhz) / (1/fTOCKI))/2
;
; Min Length = tTMR0_OverFlow = 7.8ms
; Max Length = tTMR0_OverFlow *255 = 1.99sec
;
; Temp = Freq = ((1/Fhz) / (1/fTOCKI))/ 2
;
;   82hz  = 199.804
;   500hz = 32.768
;   1khz = 16.384
;   2khz = 8.192
;
; W = #Cycles
; Beep Time = #cycles * (1/Fhz)
; for 1khz 255*(1/1000) = .255 Sec
;
; #cycles = Time in Sec / (1/fhz)
;
BEEP_675
	movlw	24		; 675hz
	movwf	Temp
	movlw	34		; 50ms
	goto	BEEP

BEEP_625
	movlw	26		; 625hz			; Min Pressed?
	movwf	Temp
	movlw	31		; 50ms
	goto	BEEP

BEEP movwf	Count
	clrf	TMR0
	bcf		Beep_Port,Beep_Pin
	movf	TMR0,W
	xorwf	Temp,W
	btfss	STATUS,Z
	 goto	$-3
	clrf	TMR0
	bsf		Beep_Port,Beep_Pin
	movf	TMR0,W
	xorwf	Temp,W
	btfss	STATUS,Z
	 goto	$-3

	decfsz	Count,F
	 goto	BEEP+1
	retlw	0

