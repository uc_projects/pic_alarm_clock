;***************************************************************************
;  Character LCD 2x16 Display (Seiko chip K-2xxxxx)
;***************************************************************************
; Execute an LCD instruction, display a character, + Shift Register
;
; Routine:              LCDIns          LCDChar         LCDInit
; Display Characters       0               1               0
; Instructions used:       41              31              5
; Registers used:       Temp         Temp
;-----------------------------------------------------------------------
;
;LCD ADDRESSES\PAGES
; **************************************************
; *00*01*02*03*04*05*06*07**08*09*10*11*12*13*14*15*	Page1
; *64*65*66*67*68*69*70*71**72*73*74*75*76*77*78*79*
; **************************************************
;
; **************************************************
; *16*17*18*19*20*21*22*23**24*25*26*27*28*29*30*31*    Page2
; *80*81*82*83*84*85*86*87**88*89*90*91*92*93*94*95*
; **************************************************
;
; *********************************
; *32 *33 *34 *35 *36 *37 *38 *39 *	Page 2.5
; *96 *97 *98 *99 *100*101*102*103*
; *********************************
;
;
;-- LCD Literals --------------
LCD_Ctrl equ PORTD
LCD_E	equ 0
LCD_RS	equ 1
LCD_RW	equ 6

LCD_Data equ PORTD
LCD_DB7 equ 2
LCD_DB6	equ 3
LCD_DB5 equ 4
LCD_DB4 equ 5

;LCD Registers
; SLCD_Address	; shadow Register for LCD Address
; SLCD_Flags	; Keeps track of Page
;Temp
;LCDCount
;LCDCountHi
;----------------------------------------
;
;  * Before the Initialization is to be executed by the PIC, 15 msecs must
;     elapse before the "Init" subroutine is called.
;
; *******************************************************************************
;LCD ADDRESSES\PAGES
; **************************************************
; *00*01*02*03*04*05*06*07**08*09*10*11*12*13*14*15*	Page1
; *64*65*66*67*68*69*70*71**72*73*74*75*76*77*78*79*
; **************************************************
;
; **************************************************
; *16*17*18*19*20*21*22*23**24*25*26*27*28*29*30*31*    Page2
; *80*81*82*83*84*85*86*87**88*89*90*91*92*93*94*95*
; **************************************************
;
; *********************************
; *32 *33 *34 *35 *36 *37 *38 *39 *	Page 2.5
; *96 *97 *98 *99 *100*101*102*103*
; *********************************
;
; *******************************************************************************
LCD_Eclk macro
    goto  	$+1				;delay 630ns
    goto  	$+1
    goto  	$+1
	bsf		LCD_Ctrl,LCD_E			;  Toggle the 'E' Clock
    goto  	$+1
    goto  	$+1
    goto  	$+1
	bcf		LCD_Ctrl,LCD_E	
 endm

;LCD_Sin
;	clrf	dly_Lo
;	btfsc	LCD_Data,LCD_DB7
;	 bsf	dly_Lo,3
;	btfsc	LCD_Data,LCD_DB6
;	 bsf	dly_Lo,2
;	btfsc	LCD_Data,LCD_DB5
;	 bsf	dly_Lo,1
;	btfsc	LCD_Data,LCD_DB4
;	 bsf	dly_Lo,0
;	retlw	0

; Initialize LCD
LCDInit
	movlw	6			;  Wait for the LCD to Power Up
	movwf	Count		; more than 30 ms
	movlw	35
    movwf	_High
	movlw	123
    movwf	_Low
	decfsz  _Low,F
     goto	$-1
  	decfsz 	_High,F
     goto	$-3
	decfsz	Count,F
	 goto	LCDInit+2
  

	movlw	b'00101000'	; Function Set, 4bit, 2line, 5x7 font
	call	LCDIns
	movlw	b'00001111'	; turn display , Blink, Cursor, on
	call	LCDIns
	movlw	b'00000110'	; Entry Mode Set, shift Cursor right, No Shift
	call	LCDIns
	movlw	b'00000001'	; Clear Display
	call	LCDIns
	goto	DispCustomChar
;	retlw	0

; *******************************************************************************
; ********** LCD Instructions ***************************************************
ShftD	movwf	Temp
		movlw	16			; W = 1 for right shift,
		addwf	Temp,W
		goto	LCDIns		; W = 0 for left shift.

ClrDisp	movlw	1
		goto	LCDIns

Line1
RetHome	movlw	2
		goto	LCDIns

Line2   movlw   64
LcdAd	movwf	Temp		; Send Cursor to addr.
		movlw	128			; W = 1 for right shift,
		addwf	Temp,W

; Send the Byte as an Instruction
LCDIns	movwf	Temp
		bcf		LCD_Ctrl,LCD_RS
		bcf		LCD_Ctrl,LCD_RW
		goto	LCDChar+5


LCDspac	movlw	' '
		goto	LCDChar

; Get Decimal Value and return W with ASCII Char
DispNum	movwf	Temp
	movlw	48
	addwf	Temp,F
	movlw	58
	subwf	Temp,W
	btfss	STATUS,C	;if C = 1 then Temp>=W
	 goto	LCDChar+1   ; so skip if set
	movlw	7
	addwf	Temp,F
	goto	LCDChar+1

; Send the Byte as Character Data
LCDChar
	movwf	Temp		;  Save the Value for the Second Nybble
	bsf		LCD_Ctrl,LCD_RS	; Make RS=1
	bcf		LCD_Ctrl,LCD_RW
	goto	$+1
	goto	$+1

	bcf		LCD_Data,LCD_DB7
	btfsc	Temp,7
	 bsf	LCD_Data,LCD_DB7
	bcf		LCD_Data,LCD_DB6
	btfsc	Temp,6
	 bsf	LCD_Data,LCD_DB6
	bcf		LCD_Data,LCD_DB5
	btfsc	Temp,5
	 bsf	LCD_Data,LCD_DB5
	bcf		LCD_Data,LCD_DB4
	btfsc	Temp,4
	 bsf	LCD_Data,LCD_DB4
	LCD_Eclk

	bcf		LCD_Data,LCD_DB7
	btfsc	Temp,3
	 bsf	LCD_Data,LCD_DB7
	bcf		LCD_Data,LCD_DB6
	btfsc	Temp,2
	 bsf	LCD_Data,LCD_DB6
	bcf		LCD_Data,LCD_DB5
	btfsc	Temp,1
	 bsf	LCD_Data,LCD_DB5
	bcf		LCD_Data,LCD_DB4
	btfsc	Temp,0
	 bsf	LCD_Data,LCD_DB4
	LCD_Eclk
	;  Wait for the LCD to Display the Character 48us
    movlw   80
    movwf   Count
	decfsz  Count,F
     goto  	$-1

	movlw	0x0FC			;  "Clear Display" and "Cursor At Home" Instructions
	andwf	Temp,W		;   Require 5 msec Delay to Complete
	btfss	STATUS,Z
	 retlw	0
	movlw	51				; wait 1.54ms for  ins to complete
	addwf	TMR0,W
	movwf	Count
	movf	TMR0,W
	xorwf	Count,W
	btfss	STATUS,Z
	 goto	$-3
	retlw	0

;-----------------------------------------------------------------------
; Display n-1 Chars in a buffer, can be used to display strings
;CDCBuff
;movlw	Buff0			; Setup Indirect Addressing
;movwf	FSR
;CDCBuffLp
;movf	INDF,W
;call	LCDChar
;incf	FSR,F			; Display next Char in buffer
;decfsz	BuffCount,F
; goto	LCDCBuffLp
;retlw	0


; Read Busy Flag -------------------------------------------
; Read Busy Flag and Address Counter
; when Bit 7 = 1, Device is busy
;BusyFlagClr
;	movlw	b'00000000'		; clear all bits
;	movwf	LCD_Data
;	movlw	b'00011110'		; Make Data Input
;	TRIS	LCD_Data
;	bcf		LCD_Ctrl,LCD_RS
;	bsf		LCD_Ctrl,LCD_RW	; read busy flag

;BFCLoop	nop
;	call	LCD_Eclk		; get high byte
;	call	LCD_Sin
;	movf	Temp,W
;	andlw	15
;	movwf	Temp			; Busy = bit3
;	swapf	Temp,F

;	call	LCD_Eclk		; get low byte
;	call	LCD_Sin
;	movf	Temp,W
;	andlw	15
;	addwf	Temp,F
;	btfsc	Temp,7
;	 goto	BFCLoop			; Loop until BF=0

;	movlw	b'00000000'		; reset the port
;	movwf	LCD_Data
;	movlw	b'00000000'		; Make Data Output
;	TRIS	LCD_Data
;	rrf		Temp,W	; shift out bit0:BF and make address-pointer valid!
;	bcf		LCD_Ctrl,LCD_RW	; read busy flag
;	retlw	0
