; *******************************************************************************
; Display the W register in binary, decimal, or hexedecimal characters.
;
; Routine:              DispDec         DispHex         DispBin
; Display Characters       3		   2		   8
; Instructions used:       22              8               11
; Registers used:       DTemp                            DTemp
;                                         DTemp          DCount
;                       DCount
;-----------------------------------------------------------------------


; *******************************************************************************
;DispBin movwf   _Low       ; MSB "00000000"  - "11111111" LSB is displayed
;		movlw	8
;     	movwf	BuffCount
;BinLp   movlw	48
;      	btfsc	_Low,7
;       	 movlw	49

;		call	LCDChar
;		rlf		_Low,F
;     	decfsz	BuffCount,F
;       	 goto	BinLp
;		retlw	0

; *******************************************************************************
BcdChar
DispHex movwf   _Low       ; "00" - "FF" is displayed
		swapf	_Low,W		; Disp MSN
		andlw	15
		call	DispNum
		movf	_Low,W		; Disp LSN
		andlw	15
		call	DispNum
		retlw	0

; *******************************************************************************
;DispDec
;		movwf   Temp       ; "001" - "255" is displayed
;		movlw	Buff0		; Setup Indirect Addressing
;		movwf	FSR
;		movlw	3			; Display 3 chars
;		movwf	BuffCount
;
;		movlw	100
;		call	DecCnt		; count Dtemp - 100
;		movlw	48				; Add 48
;		addwf	INDF,F
;		
;
;		incf	FSR,F
;		movlw	10
;		call	DecCnt		; count Dtemp - 100
;		movlw	48				; Add 48
;		addwf	INDF,F
;
;		incf	FSR,F
;		movlw	1
;		call	DecCnt		; count Dtemp - 100
;		movlw	48				; Add 48
;		addwf	INDF,F
;		retlw	0
;
;DecCnt 	movwf  	Count		; store num to be subtracted
;      	clrf   	INDF		; clear the dec counter
;moret	movf	Count,W
;      	subwf  	Temp,W		; W = DTemp - DCount
;      	btfss  	STATUS,C	; if C = 1 then DTemp>=DCount
;       	 goto   lesst		; if C = 0 then DTemp<DCount
;      	movwf  	Temp		; Save Result to DTemp Reg
;      	incf   	INDF,F
;		goto  	moret       ; Loop
;lesst	retlw	0
