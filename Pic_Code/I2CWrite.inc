;----------------------------------------------
; Write (I2Count1 - 1) bytes
;
;----------------------------------------------
I2cWrite
	movwf	I2IntAddr	; RW Bit = 0 = WRITE
	movlw	I2IntAddr	; Setup Indirect Addressing
	movwf	FSR
;	clrf	err			; This counts the number of fails
	movlw	8		; Setup Bit Counter
	movwf	Count

;-------------------------------
	I2C_Start

;------------------------------- Send Slave Addr ------------
I2CLp1
	btfsc	INDF,7		; Copy Bit, Send MSB first
	 bsf	I2c_PORT,SDA
	btfss	INDF,7
	 bcf	I2c_PORT,SDA
	I2C_CLK
	rlf		INDF,F		; Get the next bit
	decfsz	Count,F
	 goto	I2CLp1

;Wack bit -----------------------
	SDA_IN	
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	btfsc	I2c_PORT,SDA
	 goto	I2w_Afail
	bcf		I2c_PORT,SCL
	tLOW
	bcf		I2c_PORT,SDA	; Clear Data
	SDA_OUT					; SDA = output

	movlw	BuffAddr		;Does FSR = &I2Buff0 
	xorwf	FSR,W
	btfss	STATUS,Z
	 goto	$+2			; Send Address
	goto	$+5			; Send Data
	incf	FSR,F
	movlw	8			; Setup Bit Counter
	movwf	Count
	goto	I2CLp1
	movf	BuffCount,W	;if = 0 then no bytes to write
	xorlw	0
	btfsc	STATUS,Z
	 goto	I2w_Return0

;------------------ Write Data --------------------------
I2C1 movf	I2Buff0,W	; else set pointer to data
	movwf	FSR

I2C_W0
	movlw	8		; Setup Bit Counter
	movwf	Count

I2C_Wr
	btfsc	INDF,7		; Copy Bit, Send MSB first
	 bsf	I2c_PORT,SDA
	btfss	INDF,7
	 bcf	I2c_PORT,SDA
	rlf		INDF,F		; Get the next bit
	I2C_CLK
	decfsz	Count,F
	 goto	I2C_Wr

;Ack bit -----------------------
Wack
	SDA_IN	; SDA=input
	tSUDAT
	bsf		I2c_PORT,SCL	; Clock the Bit
	tHIGH
	btfsc	I2c_PORT,SDA
	 goto	I2w_Dfail
	bcf		I2c_PORT,SCL
	tLOW
	SDA_OUT	; SDA = output
	rlf		INDF,F		; Restore Register contents
;-------------------------------
	movf	FSR,W		; if Low nybble != 15 then inc
	andlw	15
	xorlw	15
	btfss	STATUS,Z
	 goto	WInD
	btfsc	FSR,4
	 goto	Wa16
WInD
	incf	FSR,F
	goto	$+3
Wa16
	movlw	17
	addwf	FSR,F
	decfsz	BuffCount,F
	 goto	I2C_W0
I2w_Return0
	tSUDAT
	I2C_Stop	;Stop Condition
	retlw	0

;-----------------------------------
;
; If fail resend data upto n times
;
;-----------------------------------
I2w_Afail
;	incf	err,F		;increment Err.counter
;	movlw	4			; retry n-1 times
;	subwf	err,W
;	btfsc	STATUS,Z
;	 goto	I2w_Return

;	SDA_OUT
;	movlw	RTC_Addr	; get slave address
;	movwf	I2IntAddr
;	movlw	8			; Setup Bit Counter
;	movwf	Count
;	goto	I2CLp1		; Retry send Address

;Send Data Fail
I2w_Dfail
;	movf	FSR,W		; save FSR
;	movwf	Temp
;	movlw	err
;	movwf	FSR
;	incf	err,F		;increment Err.counter
;	movlw	4			; retry n-1 times
;	subwf	err,W
;	btfsc	STATUS,Z
;	 goto	I2w_Return
;	movf	Temp,W		;restore FSR
;	movwf	FSR
;	goto	I2C_W0+1

I2w_Return1
	btfsc	I2c_PORT,SCL
	 bcf	I2c_PORT,SCL
	tSUDAT
	I2C_Stop	;Stop Condition
;	goto	I2c_AckErr
	retlw	1
