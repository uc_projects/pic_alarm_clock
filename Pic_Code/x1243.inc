; X1243 contains Real Time Clock and High Reliabilty 2Kx8 EEPROM
; 64 byte page Mode, and 3 bit Block Lock Protection
; Write time = 5ms, Current Write = 3ma
;
;On power
;up the internal address counter is set to address 0h, so
;a current address read of the EEPROM array starts at
;address 0
;
; STATUS REgister Default = 01h
;
;----------------------------------------------
;RTC Address Map Literals
;----------------------------------------------
; CBLOCK 112
 ; Date Literals for RTC
;Day		;equ H'36'		; 0-6
;Year	;equ H'35'		; 00-99
;Month	;equ H'34' 		; 01-12 ; century bit=7
;Date	;equ H'33'		; 01-31
 ; Time Literals for RTC
;Hour	;equ H'32'		; 1-12 + am/pm ; 00-23 ; 12/24 bit=6
;Min		;equ H'31'		; 00-59
;Sec		;equ H'30'		; 00-59

; Alarm 1
;A1DWA	;equ H'0E'
;A1Month	;equ H'0C'
;A1Date	;equ H'0B'		; 1-31/1-7 ; day/date bit=6 ; alarm bit=7
;A1Hour	;equ H'0A'		; 1-12 am/pm bit=5 ; 00-23 ; 12/24 bit=6 ; alarm bit=7
;A1Min	;equ H'09'		; 00-59 ; alarm bit=7
;A1Sec	;equ H'08' 		; 00-59 ; alarm bit=7

; Alarm 2
;A2Min	;equ H'0B' 		; 00-59 ; alarm bit=7
;A2Hour	;equ H'0C'		; 00-59 ; alarm bit=7
;A2Date	;equ H'0D'		; 1-31/1-7 ; day/date bit=6 ; alarm bit=7
; endc

WEL	equ	 1	; Write Enable Latch
RWEL equ 2	; Register Write Enable Latch
RTCF equ 0	; Real Time Clock Fail Bit
AL0	equ 5	; AL1, AL0: Alarm Bits
AL1 equ 6
;
XRTC_Addr equ b'11011110' ; slave address of device
;2048 / 64 bytes per page = 32 Pages
;
; 000-03Fh 0
; 040-07Fh 1
; 080-0BFh 2
; 0C0-0FFh 3
; 100-13Fh 4
; 140-17Fh 5
; 180-1BFh 6
; 1C0-1FFh 7
; 200-23Fh 8   9*64 = 576, 23Fh = 575+0 = 576
;
; 1024 = 400h
; 7FFh = 2047
;

XEE_Addr equ	b'10101110'

XREAD_EE

XWRITE_EE
	movwf	Temp


;(Note: Prior to writing to the CCR, the master must
;write a 02h, then 06h to the status register in two preceding
;operations to enable the write operation
;changes EEPROM values so these initiate a nonvolatile write
;cycle and will take up to 10ms to complete. Writes to
;undefined areas have no effect. The RWEL bit is
;reset by the completion of a nonvolatile write write
;cycle, so the sequence must be repeated to again
XWrite_EE_Init
	movlw	err		; Setup I2c
	movwf	FSR
	movlw	0		; The High Address = b'00000xxx' , 0-7
	movwf	BuffAddr
	movlw	I2Buff1	; use the I2c Buffer
	movwf	I2Buff0
	movlw	H'3F'	; This is the Low Address
	movwf	I2Buff1
	movlw	2		; This is the Data
	movwf	I2Buff2
	movlw	2			; Number Of bytes to write
	movwf	BuffCount
	movlw	XWrite_EE_Init+2	; Address for Retry
	movwf	_Low
	movlw	XWrite_EE_Init+18
	movwf	_High
	movlw	XEE_Addr
	goto	I2cWrite

XWrite_EE_Init2
	movlw	6		; This is the Data
	movwf	I2Buff2
	movlw	2			; Number Of bytes to write
	movwf	BuffCount
	movlw	XWrite_EE_Init2	; Address for Retry
	movwf	_Low
	movlw	XWrite_EE_Init2+10
	movwf	_High
	movlw	XEE_Addr
	call	I2cWrite
	retlw	0



