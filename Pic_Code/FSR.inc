; if any of the page boundries are reached add 16 and continue
;increment the FSR and return
IncFSR
;	movf	FSR,W
;	xorlw	FSRStop
;	btfsc	STATUS,Z
;	 goto	$+3
;	movlw	FSRStart,W	; Reset FSR to start
;	movwf	FSR

	movf	FSR,W		; if Low nybble != 15 then inc
	andlw	15
	xorlw	15
	btfss	STATUS,Z
	 goto	InD
	btfsc	FSR,4
	 goto	a16

InD	incf	FSR,F
	movf	_Status,W	; Restore Page Bits
	movwf	STATUS
	retlw	0
a16	movlw	17
	addwf	FSR,F
	movf	_Status,W	; Restore Page Bits
	movwf	STATUS
	retlw	0

; --------------------------------------------------------
;  Indirect Addressing for PicF59
; there are 8 banks of 16 registers, with 16 registers
; that wrap to 00h-0Fh at the end of each bank.
; any regster with an address greater than 1Fh has to use indirect addressing
ClearFSR
	movwf	FSR
;	movlw	1;
;	movwf	Temp;
	movf	Temp,W
	movwf	INDF

LP1	call	IncFSR
	movf	Temp,W
;	incf	Temp,F;
	movwf	INDF
	movf	FSR,W	; Loop while not equal to 0
	xorwf	Count,W
	btfss	STATUS,Z
	 goto	LP1
	retlw	0
