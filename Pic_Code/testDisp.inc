;----------------------------------------------------------------------
; Display String
	bcf		STATUS,PA0	; Page0
	movlw	Buff0
	movwf	FSR
	movlw	'P'
	movwf	INDF
	incf	FSR,F
	movlw	'1'
	movwf	INDF
	incf	FSR,F
	movlw	'6'
	movwf	INDF
	incf	FSR,F
	movlw	'F'
	movwf	INDF
	incf	FSR,F
	movlw	'5'
	movwf	INDF
	incf	FSR,F
	movlw	'9'
	movwf	INDF
	movlw	6
	movwf	BuffCount

; Call the LCD Stuff
	bsf		STATUS,PA0	; Page1
	call	LCDCBuff
	call	Line2
	bcf		STATUS,PA0	; Page0

	movlw	Buff0
	movwf	FSR
	movlw	'M'
	movwf	INDF
	incf	FSR,F
	movlw	'i'
	movwf	INDF
	incf	FSR,F
	movlw	'c'
	movwf	INDF
	incf	FSR,F
	movlw	'r'
	movwf	INDF
	incf	FSR,F
	movlw	'o'
	movwf	INDF
	incf	FSR,F
	movlw	'C'
	movwf	INDF
	incf	FSR,F
	movlw	'H'
	movwf	INDF
	incf	FSR,F
	movlw	'i'
	movwf	INDF
	incf	FSR,F
	movlw	'p'
	movwf	INDF
	movlw	9
	movwf	BuffCount

	bsf		STATUS,PA0	; Page1
	call	LCDCBuff
	bcf		STATUS,PA0	; Page0

	movlw	200		; wait 1 Secound
	movwf	Count
	call	Dlay5ms
	decfsz	Count,F
	 goto	$-2
;
;DispDec Convert W in to number characters
;in a buffer and display the buffer
	movlw	249			; Display Decimal
	call	DispDec
	call	LCDCBuff

	movlw	249
	call	DispHex
	call	LCDCBuff

	movlw	249
	call	DispBin
	call	LCDCBuff

; Blink the LED
Lp1a bsf	RTC_PORT,RTC_ALARM
	movlw	10
	movwf	Count
	call	Dlay5ms
	decfsz	Count,F
	 goto	$-2
	bcf		RTC_PORT,RTC_ALARM
	movlw	10
	movwf	Count
	call	Dlay5ms
	decfsz	Count,F
	 goto	$-2
	goto	Lp1a
