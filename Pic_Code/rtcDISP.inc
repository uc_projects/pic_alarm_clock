;
; 12:01.23pm_40�C?
; Mon Jul 22, 2008
;

LCDaddStart
	movlw	RoutineSel
	movwf	FSR
;	movlw	7			; make sure value is within range
;	subwf	RoutineSel,W
;	btfss	STATUS,C
;	 goto	$+3
;	movlw	0
;	goto	$+2
	movf	RoutineSel,W
	addwf	PCL,F
	retlw	76	;	goto	IncYear	 ;6
	retlw	68	;	goto	IncMonth ;5
	retlw	72	;	goto	IncDate	 ;4
	retlw	64	;	goto	IncDay	 ;3
	retlw	0	;	goto	IncHour  ;0
	retlw	3	;	goto	IncMin	 ;1
	retlw	5	;	goto	IncSec	 ;2


;--------------------------------
; Month of the year table
;Jan 1
;Feb
;Mar
;Apr
;May
;Jun
;Jul
;Aug
;Sep
;Oct
;Nov
;Dec 12
MOY;	movlw	36
;	subwf	_Low,W
;	btfss	STATUS,C
;	 goto	$+3
;	movlw	0
;	goto	$+2
	movf	_Low,W
	addwf	PCL,F
	retlw	'J'
	retlw	'a'
	retlw	'n'
	retlw	'F'
	retlw	'e'
	retlw	'b'
	retlw	'M'
	retlw	'a'
	retlw	'r'
	retlw	'A'
	retlw	'p'
	retlw	'r'
	retlw	'M'
	retlw	'a'
	retlw	'y'
	retlw	'J'
	retlw	'u'
	retlw	'n'
	retlw	'J'
	retlw	'u'
	retlw	'l'
	retlw	'A'
	retlw	'u'
	retlw	'g'
	retlw	'S'
	retlw	'e'
	retlw	'p'
	retlw	'O'
	retlw	'c'
	retlw	't'
	retlw	'N'
	retlw	'o'
	retlw	'v'
	retlw	'D'
	retlw	'e'
	retlw	'c'

;--------------------------------
;  Day Of the Week TAble
;Sun 1
;Mon 2
;Tue 3
;Wen 4
;Thu 5
;Fri 6
;Sat 7
DOW;	movlw	21			; make sure value is within range
;	subwf	_Low,W
;	btfss	STATUS,C
;	 goto	$+3
;	movlw	0
;	goto	$+2
	movf	_Low,W
	addwf	PCL,F
	retlw	'S' ;0|1-	(n-1)*3
	retlw	'u'
	retlw	'n'
	retlw	'M' ;3|2-   
	retlw	'o'
	retlw	'n'
	retlw	'T'	;6|3-   
	retlw	'u'
	retlw	'e'
	retlw	'W' ;9|4-   
	retlw	'e'
	retlw	'n'
	retlw	'T' ;12|5-
	retlw	'h'
	retlw	'u'
	retlw	'F' ;15|6-
	retlw	'r'
	retlw	'i'
	retlw	'S' ;18|7-
	retlw	'a'
	retlw	't'

; Hardware Includes --------------------------------
CalcTable
	movwf	_Low
	decf	_Low,F	;(_Low-1)*3
	movf	_Low,W
	addwf	_Low,F
	addwf	_Low,F
	retlw	0

rtcDisp
	movlw	RoutineSel
	movwf	FSR
;	movlw	7			; make sure value is within range
;	subwf	RoutineSel,W
;	btfss	STATUS,C
;	 goto	$+3
;	movlw	0
;	goto	$+2
	movf	RoutineSel,W
	addwf	PCL,F
	goto	DispYear	 ;6
	goto	DispMonth ;5
	goto	DispDate	 ;4
	goto	DispDay	 ;3
	goto	DispHr  ;0
	goto	DispMin	 ;1
	goto	DispSec	 ;2

;------------------------------------------------------------
;  Display Time encoded in BCD Format
; The Registers start at Buff32
;
; Display 12:00 am
; or 	  24:00		Depending on format
;
; This takes 6 bits in BCD format and converts to ASCII
;Numbers stored in the buffer.
; bits 4-7 = high number and bits 0-3 = low number
;
; BCD Number range from 0 to 9??????
DispTemp ;-----------------------------------------------
; call ConvertTemp
; DisplayTemp in MSB_Temp, which is in BCD format
	movlw	MSB_Temp
	movwf	FSR
	swapf	MSB_Temp,W
	andlw	15
	call	DispNum
	movf	MSB_Temp,W
	andlw	15
	call	DispNum
	swapf	LSB_Temp,W
	andlw	15
	call	DispNum
	movf	LSB_Temp,W
	andlw	15
	call	DispNum
	retlw	0

DispTime ;------------------------------------
; Always 12hour mode
DispHr
	movlw	Hour	 ;XX:
	movwf	FSR
	movlw	'1'
	btfss	Hour,4	; if 10 hour NOT set then disp space
	 movlw	' '
	call	LCDChar
	movf	Hour,W
	andlw	15
	call	DispNum
	movlw	':'
	call	LCDChar

Dispampm
	movlw	8			;am/pm LCD address
	call	LcdAd
	movlw	'a'
	btfsc	Hour,5		; set = pm
	 movlw	'p'
	call	LCDChar
	movlw	'm'
	call	LCDChar
	movlw	3			; Min LCD address
	call	LcdAd
	retlw	0

DispSec				;.XX
	movlw	'.'
	call	LCDChar
	movlw	Sec
	goto	$+2
DispMin
	movlw	Min		;XX
	movwf	FSR
	swapf	INDF,W		; Disp MSN
	andlw	15
	call	DispNum
	movf	INDF,W		; Disp LSN
	andlw	15
	call	DispNum
	retlw	0

;------------------------------------------------------
; Thu Sep 21, 2009 = 16 chars
;DispDate
DispDay
	movlw	3
	movwf	_High
	movlw	Day
	movwf	FSR
	movf	Day,W
	call	CalcTable
	call	DOW		;J
	call	LCDChar
	incf	_Low,F
	decfsz	_High,F
	 goto	$-4
	movlw	','
	call	LCDChar
	retlw	0

DispMonth
	movlw	3
	movwf	_High
	movlw	Month
	movwf	FSR
	movf	Month,W		; mask out low nybble
	andlw	15
	movwf	_Low
	movlw	10
	btfsc	Month,4		; if bit4 is set add 10 to month
	 addwf	_Low,F
	movf	_Low,W
	call	CalcTable
	call	MOY		;J
	call	LCDChar
	incf	_Low,F
	decfsz	_High,F
	 goto	$-4
	call	LCDspac
	retlw	0

DispDate
	movlw	Date
	movwf	FSR
	swapf	Date,W		; Disp MSN
	andlw	15
	call	DispNum
	movf	Date,W		; Disp LSN
	andlw	15
	call	DispNum
	movlw	','
	call	LCDChar
	call	LCDspac
	retlw	0

DispYear
	movlw	Year
	movwf	FSR
	movlw	'2'
	call	LCDChar
	movlw	'0'
	call	LCDChar
	swapf	Year,W		; Disp MSN
	andlw	15
	call	DispNum
	movf	Year,W		; Disp LSN
	andlw	15
	call	DispNum
	retlw	0

