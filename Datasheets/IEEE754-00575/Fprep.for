      PROGRAM FPREP
C
      IMPLICIT NONE
C
      REAL*8 X,Y,Z
      REAL*4 W
      INTEGER*4 IW,IU
      INTEGER*1 B(4),C(4)
      EQUIVALENCE(W,B(1))
      EQUIVALENCE(W,IW)
      EQUIVALENCE(IU,C(1))
C
      INTEGER K,N,P,A(32)
      N=32
C
1     PRINT 100
100   FORMAT(1X,'ENTER Y: ',$)
      READ *,Y
      W=SNGL(Y)
C
      Z=DLOG(DABS(Y))/DLOG(2.D0)
      P=INT(Z)
      IF(DBLE(P).GT.Z)P=P-1
      X=DABS(Y)*2.D0**(-P)
      DO K=1,N
         IF(X.GE.2.D0**(-K+1))THEN
            A(K)=1
         ELSE
            A(K)=0
         ENDIF
      X=X-DBLE(A(K))*2.D0**(-K+1)
      ENDDO
C
      PRINT 300,P,A(1),(A(K),K=2,N)
300   FORMAT(1X,'e = ',I4,3X,'f = ',I1,'.',3I1,1X,7(4I1,1X))
C
      PRINT 200,W
200   FORMAT(1X,'IEEE FORMAT: ',Z8)
C
      C(1)=B(1)
      C(2)=B(2)
      C(3)=B(3)
      IW=ISHC(IW,1)
      C(4)=B(4)
      IF(Y.LT.0.)THEN
          IU=IBSET(IU,23)
      ELSE
          IU=IBCLR(IU,23)
      ENDIF
      PRINT 400,IU
400   FORMAT(1X,'PIC  FORMAT: ',Z8)
C
      GOTO  1
C
      END

